﻿using System;


namespace TicketBookingOnline.ViewModels.LichChieus
{
    public class CreateLichChieuViewModel
    {
        public int PhimId { get; set; }

        public int RapId { get; set; }

        public DateTime XuatChieu { get; set; }

        public int PhanTramKM { get; set; }

    }
}
