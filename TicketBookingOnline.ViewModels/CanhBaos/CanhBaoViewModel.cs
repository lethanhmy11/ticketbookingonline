﻿using System.ComponentModel;

namespace TicketBookingOnline.ViewModels.CanhBaos
{
    public class CanhBaoViewModel
    {
        public int CanhBaoId { get; set; }

        [DisplayName("Tên cảnh báo")]
        public string TenCanhBao { get; set; }

        [DisplayName("Ảnh")]
        public string Anh { get; set; }
    }
}
