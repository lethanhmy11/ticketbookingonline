﻿using System;
using System.Collections.Generic;
using TicketBookingOnline.ViewModels.ChiTietVes;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.LichChieus;
using TicketBookingOnline.ViewModels.RapGhes;

namespace TicketBookingOnline.ViewModels.Ves
{
    public class VeDetailViewModel
    {
        public int VeId { get; set; }
        public int RapId { get; set; }
        public int LichChieuId { get; set; }
        public DateTime NgayDatVe { get; set; }
        public decimal TongGiaVe { get; set; }
        public string TrangThai { get; set; }
        public int UserId { get; set; }

        public List<ChiTietVeViewModel> ChiTietVes { get; set; }
        public RapGheViewModel RapGhe { get; set; }
        public LichChieuViewModel LichChieu { get; set; }
        public KhachHangViewModel User { get; set; }

        public string TenCumRap { get; set; }

        public string TenRap { get; set; }

        public string DiaChi { get; set; }
    }
}
