﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using TicketBookingOnline.ViewModels.ChiTietVes;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.LichChieus;


namespace TicketBookingOnline.ViewModels.Ves
{
    public class VeViewModel
    {
        public int VeId { get; set; }

        public int RapId { get; set; }

        [DisplayName("Ngày đặt vé")]
        public DateTime NgayDatVe { get; set; }

        [DisplayName("Tổng giá vé")]
        public decimal TongGiaVe { get; set; }

        [DisplayName("Trạng thái")]
        public string TrangThai { get; set; }

        public int UserId { get; set; }

        public List<ChiTietVeViewModel> ChiTietVes { get; set; }

        public LichChieuViewModel LichChieu { get; set; }

        public KhachHangViewModel User { get; set; }


    }
}
