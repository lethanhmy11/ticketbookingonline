﻿using System;
using System.Collections.Generic;
using TicketBookingOnline.ViewModels.ChiTietVes;

namespace TicketBookingOnline.ViewModels.Ves
{
    public class CreateVeViewModel
    {
        //public int RapId { get; set; }
        
        public int LichChieuId { get; set; }

        public DateTime NgayDatVe { get; set; }
        public decimal TongGiaVe { get; set; }
        public string TrangThai { get; set; }
        public int UserId { get; set; }
        public string Note { get; set; }
        public List<ChiTietVeViewModel> ChiTietVes{get;set;}
    }
}
