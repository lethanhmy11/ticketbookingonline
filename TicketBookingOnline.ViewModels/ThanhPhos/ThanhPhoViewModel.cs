﻿using System.ComponentModel;

namespace TicketBookingOnline.ViewModels.ThanhPhos
{
    public class ThanhPhoViewModel
    {
        public int ThanhPhoId { get; set; }

        [DisplayName("Thành phố")]
        public string TenThanhPho { get; set; }
    }
}
