﻿using System.ComponentModel.DataAnnotations;

namespace TicketBookingOnline.ViewModels.Accounts
{
    public class LoginViewModel
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public int CumRapId { get; set; }

        public string TenRap { get; set; }

        public bool RememberMe { get; set; }
    }
}
