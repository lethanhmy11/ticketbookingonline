﻿using System.ComponentModel;


namespace TicketBookingOnline.ViewModels.TheLoais
{
    public class TheLoaiViewModel
    {
        public int TheLoaiId { get; set; }

        [DisplayName("Tên thể loại")]
        public string TenTheLoai { get; set; }
    }
}
