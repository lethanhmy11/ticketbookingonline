﻿namespace TicketBookingOnline.ViewModels.RapGhes
{
    public class UpdateRapGheViewModel
    {
        public int? RapId { get; set; }

        public decimal? GiaGhe { get; set; }

        public int? SoLuongGhe { get; set; }

        public int LoaiGheId { get; set; }
    }
}
