﻿using TicketBookingOnline.ViewModels.LoaiGhes;

namespace TicketBookingOnline.ViewModels.RapGhes
{
    public class RapGheViewModel
    {
        public int ? RapId { get; set; }

        public decimal ? GiaGhe { get; set; }

        public int ? SoLuongGhe { get; set; }

        public int LoaiGheId { get; set; }

        public LoaiGheViewModel LoaiGhe { get; set; }
    }
}
