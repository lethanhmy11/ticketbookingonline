﻿using System;
using System.ComponentModel;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.Phims;

namespace TicketBookingOnline.ViewModels.Comments
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }

        public int UserId { get; set; }

        public int PhimId { get; set; }

        [DisplayName("Nội dung")]
        public string NoiDung { get; set; }

        public int Rate { get; set; }

        [DisplayName("Thời gian")]
        public DateTime ThoiGian { get; set; }

        public PhimViewModel Phim { get; set; }

        public KhachHangViewModel User { get; set; }
    }
}
