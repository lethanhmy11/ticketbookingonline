﻿using System.ComponentModel;

namespace TicketBookingOnline.ViewModels.Comments
{
    public class CreateCommentViewModel
    {
        public int UserId { get; set; }

        public int PhimId { get; set; }

        [DisplayName("Nội dung")]
        public string NoiDung { get; set; }
    }
}
