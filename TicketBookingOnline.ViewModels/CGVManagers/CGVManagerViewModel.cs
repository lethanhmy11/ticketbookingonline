﻿using System.ComponentModel;

namespace TicketBookingOnline.ViewModels.CGVManagers
{
    public class CGVManagerViewModel
    {
        public string Id { get; set; }

        [DisplayName("Họ và tên")]
        public string HoTen { get; set; }

        [DisplayName("Tên tài khoản")]
        public string UserName { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Số điện thoại")]
        public string PhoneNumber { get; set; }

        public int CumRapId { get; set; }

        [DisplayName("Tên cụm rạp")]
        public string TenCumRap { get; set; }

        [DisplayName("Địa chỉ")]
        public string DiaChi { get; set; }

        [DisplayName("Role")]
        public string RoleName { get; set; }
    }
}
