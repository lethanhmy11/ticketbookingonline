﻿namespace TicketBookingOnline.ViewModels.CGVManagers
{
    public class LoginCGVManagerViewModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
