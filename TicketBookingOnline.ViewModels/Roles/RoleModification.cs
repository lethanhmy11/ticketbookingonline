﻿using System.ComponentModel.DataAnnotations;

namespace TicketBookingOnline.ViewModels.Roles
{
    public class RoleModification
    {
        [Required]
        public string RoleName { get; set; }

        public string RoleId { get; set; }
        
        public string[]? AddIds { get; set; }
        
        public string[]? DeleteIds { get; set; }
    }
}
