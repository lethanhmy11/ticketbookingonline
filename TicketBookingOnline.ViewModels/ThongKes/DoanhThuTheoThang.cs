﻿using TicketBookingOnline.ViewModels.CumRaps;

namespace TicketBookingOnline.ViewModels.ThongKes
{
    public class DoanhThuTheoThang
    {
        public int ? CumRapId { get; set; }

        public string Thang { get; set; }

        public decimal DoanhThu { get; set; }

        public CumRapViewModel CumRap { get; set; }
    }
}
