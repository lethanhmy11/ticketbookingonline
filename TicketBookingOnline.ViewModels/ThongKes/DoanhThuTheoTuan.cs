﻿namespace TicketBookingOnline.ViewModels.ThongKes
{
    public class DoanhThuTheoTuan
    {
        public int? CumRapId { get; set; }

        public string Tuan { get; set; }

        public decimal DoanhThu { get; set; }
    }
}
