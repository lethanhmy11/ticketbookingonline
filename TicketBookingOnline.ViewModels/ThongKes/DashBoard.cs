﻿namespace TicketBookingOnline.ViewModels.ThongKes
{
    public class DashBoard
    {
        public int ? CumRapId { get; set; }

        public int SoLuongVeMoi { get; set; }

        public int SoLuongKhachHangMoi { get; set; }

        public int SoLuongBinhLuanMoi { get; set; }

        public decimal DoanhThuNgay { get; set; }

        public decimal DoanhThuTuan { get; set; }
    }
}
