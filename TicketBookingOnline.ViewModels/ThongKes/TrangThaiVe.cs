﻿namespace TicketBookingOnline.ViewModels.ThongKes
{
    public class TrangThaiVe
    { 
        public int ? CumRapId { get; set; }
        public string TrangThai { get; set; }
        public int SoLuong { get; set; }
    }
}
