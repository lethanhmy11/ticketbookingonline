﻿namespace TicketBookingOnline.ViewModels.ChiTietVes
{
    public class ChiTietVeViewModel
    {
        public int ChiTietVeId { get; set; }

        public int LoaiGheId { get; set; }

        public string ViTri { get; set; }

        public int VeId { get; set; }

        public int RapId { get; set; }

    }
}
