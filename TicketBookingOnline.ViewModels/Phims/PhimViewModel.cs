﻿using System;
using System.ComponentModel;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.ViewModels.Phims
{
    public class PhimViewModel
    {
        public int PhimId { get; set; }

        [DisplayName("Tên Phim")]
        public string TenPhim { get; set; }

        [DisplayName("Poster")]
        public string Anh { get; set; }

        [DisplayName("Trailer")]
        public string Trailer { get; set; }

        [DisplayName("Thể loại")]
        public string TheLoais { get; set; }

        [DisplayName("Thời lượng")]
        public int ThoiLuong { get; set; }

        [DisplayName("Khởi chiếu")]
        public DateTime KhoiChieu { get; set; }

        [DisplayName("Cảnh báo")]
        public CanhBao CanhBao { get; set; }

        public string TrangThai { get; set; }
        public float TotalRate { get; set; }
        public float KhuyenMai { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}
