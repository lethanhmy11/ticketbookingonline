﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static TicketBookingOnline.Core.Models.Phim;

namespace TicketBookingOnline.ViewModels.Phims
{
    public class CreatePhimViewModel
    {
        public int PhimId { get; set; }

        [DisplayName("Tên phim")]
        [Required(ErrorMessage="Tên phim không được để trống")]
        [StringLength(255, ErrorMessage = "Tên phim không được quá 255 ký tự")]
        public string TenPhim { get; set; }

        [DisplayName("Thời lượng")]
        [Required(ErrorMessage = "Thời lượng không được để trống")]
        public int ThoiLuong { get; set; }

        [DisplayName("Ảnh")]
        //[Required(ErrorMessage = "Ảnh không được để trống")]
        public string Anh { get; set; }

        [DisplayName("Banner")]
        //[Required(ErrorMessage = "Banner không được để trống")]
        public string Banner { get; set; }

        [DisplayName("Trailer")]
        //[Required(ErrorMessage = "Trailer không được để trống")]
        public string Trailer { get; set; }

        [DisplayName("Mô tả")]
        //[Required(ErrorMessage = "Mô tả không được để trống")]
        public string MoTa { get; set; }

        [DisplayName("Ngày khởi chiếu")]
        [Required(ErrorMessage = "Ngày khởi chiếu không được để trống")]

        public DateTime KhoiChieu { get; set; }

        [DisplayName("Ngôn ngữ")]
        [Required(ErrorMessage = "Ngôn ngữ không được để trống")]
        public string NgonNgu { get; set; }

        [DisplayName("Đạo diễn")]
        [Required(ErrorMessage = "Đạo diễn không được để trống")]
        [StringLength(255, ErrorMessage = "Tên đạo diễn không được quá 255 ký tự")]
        public string DaoDien { get; set; }

        [DisplayName("Diễn viên")]
        [Required(ErrorMessage = "Diễn viên không được để trống")]
        [StringLength(255, ErrorMessage = "Tên diễn viên không được quá 255 ký tự")]
        public string DienVien { get; set; }

        [DisplayName("Trạng thái")]
        //public TrangThaiPhim TrangThai { get; set; }
        public string TrangThai { get; set; }

        [DisplayName("Giá vé")]
        [Required(ErrorMessage = "Giá vé không được để trống")]
        public decimal GiaVe { get; set; }

        [DisplayName("Khuyến mãi")]
        public int KhuyenMai { get; set; }

        [DisplayName("Thể loại")]
        [Required(ErrorMessage = "Thể loại không được để trống")]
        public string TheLoais { get; set; }

        [DisplayName("Cảnh báo")]
        [Required(ErrorMessage = "Cảnh báo không được để trống")]
        public int CanhBaoId { get; set; }

        public bool IsActive { get; set; }
    }
}
