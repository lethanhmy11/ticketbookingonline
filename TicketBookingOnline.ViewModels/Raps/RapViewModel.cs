﻿using System.ComponentModel;


namespace TicketBookingOnline.ViewModels.Raps
{
    public class RapViewModel
    {
        public int RapId { get; set; }

        [DisplayName("Cụm rạp")]
        public int CumRapId { get; set; }

        [DisplayName("Loại rạp")]
        public string LoaiRap { get; set; }

        [DisplayName("Tên rạp")]
        public string TenRap { get; set; }
    }
}
