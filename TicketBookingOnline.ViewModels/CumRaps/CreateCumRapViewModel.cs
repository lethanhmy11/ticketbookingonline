﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TicketBookingOnline.ViewModels.CumRaps
{
    public class CreateCumRapViewModel
    {
        public int CumRapId { get; set; }

        [DisplayName("Tên thành phố")]
        [Required(ErrorMessage = "Thành phố không được để trống")]
        public int ThanhPhoId { get; set; }

        [DisplayName("Tên rạp")]
        [Required(ErrorMessage = "Tên rạp không được để trống")]
        public string TenRap { get; set; }

        [DisplayName("Địa chỉ")]
        [Required(ErrorMessage = "Địa chỉ không được để trống")]
        public string DiaChi { get; set; }

        [DisplayName("Số điện thoại")]
        [Required(ErrorMessage = "Số điện thoại không được để trống")]
        public string SoDienThoai { get; set; }
    }
}
