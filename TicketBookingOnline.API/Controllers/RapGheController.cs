﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.RapGhes;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RapGheController : ControllerBase
    {
        private readonly IRapGheServices _rapGheServices;

        public RapGheController(IRapGheServices rapGheServices)
        {
            _rapGheServices = rapGheServices;
        }

        [HttpGet]
        [Route("GetRapGheByRap/{rapId}")]
        public IActionResult GetRapGheByRap(int rapId)
        {
            if(rapId == null)
            {
                return BadRequest();
            }
            var rapGhes = _rapGheServices.GetRapGheByRap(rapId);
            return Ok(rapGhes);
        }

        [HttpPatch]
        [Route("UpdateRange")]
        public IActionResult UpdateRange(List<RapGheViewModel> rapGhes)
        {
            if (rapGhes == null)
            {
                return BadRequest();
            }
            var response = _rapGheServices.UpdateRange(rapGhes);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

    }
}
