﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.Comments;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentServices _commentServices;

        public CommentController(ICommentServices commentServices)
        {
            _commentServices = commentServices;
        }

        [HttpGet]
        [Route("ConfirmBinhLuan/{commentId}")]
        public IActionResult ConfirmBinhLuan(int commentId)
        {
            var response = _commentServices.ConfirmComment(commentId);
            if (!response)
            {
                ModelState.AddModelError($"", "Confirm error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpGet]
        [Route("DeleteBinhLuan/{commentId}")]
        public IActionResult DeleteBinhLuan(int commentId)
        {
            var response = _commentServices.DeleteComment(commentId);
            if (!response)
            {
                ModelState.AddModelError($"", "Delete error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpGet]
        [Route("GetBinhLuanPhim/{phimId}")]
        public IActionResult BinhLuanPhim(int phimId)
        {
            var response = _commentServices.GetBinhLuanByPhimId(phimId);
            return Ok(response);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(CreateCommentViewModel comment)
        {
            if (comment == null)
                return BadRequest();
            var response = _commentServices.Create(comment);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error !");
                return StatusCode(500, ModelState);
            }
            var lastCmt = _commentServices.GetCommentLast(comment.UserId);
            return Ok(lastCmt);
        }
    }
}
