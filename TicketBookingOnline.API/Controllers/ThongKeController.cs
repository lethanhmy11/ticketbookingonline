﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.ThongKes;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThongKeController : ControllerBase
    {
        private readonly IThongKeServices _thongKeServices;
        private readonly ICumRapServices _cumRapServices;

        public ThongKeController(IThongKeServices thongKeServices, ICumRapServices cumRapServices)
        {
            _thongKeServices = thongKeServices;
            _cumRapServices = cumRapServices;
        }

        #region CGV

        [HttpGet]
        [Route("DashBoard")]
        public IActionResult DashBoard(int? cumRapId)
        {
            return Ok(_thongKeServices.DashBoard(cumRapId));
        }

        [HttpGet]
        [Route("DoanhThuNgay/{ngay}")]
        public IActionResult DoanhThuNgay(int? cumRapId, DateTime ngay)
        {
            if (ngay == null)
            {
                return BadRequest();
            }
            var doanhThu = _thongKeServices.DoanhThuNgay(cumRapId, ngay);
            return Ok(doanhThu);
        }

        [HttpGet]
        [Route("DoanhThuTuan/{ngay}")]
        public IActionResult DoanhThuTuan(int? cumRapId, DateTime ngay)
        {
            if (ngay == null)
            {
                return BadRequest();
            }
            var doanhThu = _thongKeServices.DoanhThuTuan(cumRapId, ngay);
            return Ok(doanhThu);
        }

        [HttpGet]
        [Route("DoanhThuThang/{thang}/{nam}")]
        public IActionResult DoanhThuThang(int? cumRapId, int thang, int nam)
        {
            if (thang == null || nam == null)
            {
                return BadRequest();
            }
            var doanhThu = _thongKeServices.DoanhThuThang(cumRapId, thang, nam);
            return Ok(doanhThu);
        }

        [HttpGet]
        [Route("GetAllDoanhThuNgayOfThangInCumRap/{cumRapId}/{thang}/{nam}")]
        public IActionResult GetAllDoanhThuNgayOfThangInCumRap(int cumRapId, int thang, int nam)
        {
            if (thang == null || nam == null || cumRapId == null)
            {
                return BadRequest();
            }
            var doanhThu = _thongKeServices.GetAllDoanhThuNgayOfThangInCumRap(cumRapId, thang, nam);
            return Ok(doanhThu);
        }

        [HttpGet]
        [Route("ThongKeDoanhThuTuan/{ngay}")]
        public IActionResult ThongKeDoanhThuTuan(int? cumRapId, DateTime ngay)
        {
            if (ngay == null)
            {
                return BadRequest();
            }
            //ngay = new DateTime(2022, 4, 11);
            var doanhThus = _thongKeServices.ThongKeDoanhThuTuan(cumRapId, ngay);
            return Ok(doanhThus);
        }

        [HttpGet]
        [Route("ThongKeDoanhThuNam/{nam}")]
        public IActionResult ThongKeDoanhThuNam(int? cumRapId, int nam)
        {
            if (nam == null)
            {
                return BadRequest();
            }
            //ngay = new DateTime(2022, 4, 11);
            var doanhThus = _thongKeServices.ThongKeDoanhThuNam(cumRapId, nam);
            return Ok(doanhThus);
        }

        [HttpGet]
        [Route("ThongKeTrangThaiVe/{ngay}")]
        public IActionResult ThongKeTrangThaiVe(int? cumRapId, DateTime ngay)
        {
            if (ngay == null)
            {
                return BadRequest();
            }
            //ngay = new DateTime(2022, 4, 11);
            var doanhThus = _thongKeServices.ThongKeTrangThaiVe(cumRapId, ngay);
            return Ok(doanhThus);
        }

        #endregion

        #region Admin

        [HttpGet]
        [Route("GetAllDoanhThuNgay/{ngay}")]
        public IActionResult GetAllDoanhThuNgay(DateTime ngay, int thanhPhoId)
        {
            if (ngay == null)
            {
                return BadRequest();
            }
            var doanhThus = _thongKeServices.GetAllDoanhThuNgay(ngay);
            var ttttt = doanhThus.ToList();
            if(thanhPhoId != null && thanhPhoId != 0)
            {
                doanhThus = doanhThus.Where(x => x.CumRap.ThanhPhoId == thanhPhoId );
            }
            var ttt = doanhThus.ToList();
            return Ok(doanhThus);
        }


        [HttpGet]
        [Route("GetAllDoanhThuThang/{thang}/{nam}")]
        public IActionResult GetAllDoanhThuThang(int thang, int nam, int thanhPhoId)
        {
            if (thang == null || nam == null)
            {
                return BadRequest();
            }
            var cumRaps = _cumRapServices.GetAll(null, thanhPhoId);
            var doanhThus = new List<DoanhThuTheoThang>();
            foreach (var item in cumRaps)
            {
                var doanhThu = _thongKeServices.DoanhThuThang(item.CumRapId, thang, nam);
                doanhThus.Add(doanhThu);
            }

            return Ok(doanhThus.OrderBy(x => x.CumRap.ThanhPhoId));
        }

        [HttpGet]
        [Route("GetAllDoanhThuNam/{nam}")]
        public IActionResult GetAllDoanhThuNam(int nam, int thanhPhoId)
        {
            if (nam == null)
            {
                return BadRequest();
            }
            var cumRaps = _cumRapServices.GetAll(null, thanhPhoId);
            var doanhThus = new List<DoanhThuTheoNam>();
            foreach (var item in cumRaps)
            {
                var doanhThu = _thongKeServices.DoanhThuNam(item.CumRapId, nam);
                doanhThus.Add(doanhThu);
            }

            return Ok(doanhThus.OrderBy(x => x.CumRap.ThanhPhoId));
        }

        [HttpGet]
        [Route("GetKhachHangMoi")]
        public IActionResult GetKhachHangMoi(int? cumRapId)
        {
            var khachHangs = _thongKeServices.GetKhachHangMoi(cumRapId);
            return Ok(khachHangs);
        }

        [HttpGet]
        [Route("GetBinhLuanMoi")]
        public IActionResult GetBinhLuanMoi()
        {
            var comments = _thongKeServices.GetBinhLuanMoi();
            return Ok(comments);
        }
        #endregion
    }
}
