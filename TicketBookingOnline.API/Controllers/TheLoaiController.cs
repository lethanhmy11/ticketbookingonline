﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels.TheLoais;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TheLoaiController : ControllerBase
    {
        private readonly ITheLoaiServices _theLoaiServices;

        public TheLoaiController(ITheLoaiServices theLoaiServices)
        {
            _theLoaiServices = theLoaiServices;
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var theloais = _theLoaiServices.GetAll();
            return Ok(theloais);
        }

        [HttpGet]
        [Route("GetById/{theLoaiId}")]
        public IActionResult GetById(int theLoaiId)
        {
            var theLoai = _theLoaiServices.GetById(theLoaiId);
            return Ok(theLoai);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(TheLoaiViewModel theLoai)
        {
            if(theLoai == null)
            {
                return BadRequest();
            }
            var response = _theLoaiServices.Create(theLoai);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Create Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpPatch]
        [Route("Update")]
        public IActionResult Update(TheLoaiViewModel theLoai)
        {
            if (theLoai == null)
            {
                return BadRequest();
            }
            var response = _theLoaiServices.Update(theLoai);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("Delete/{theLoaiId}")]
        public IActionResult Delete(int theLoaiId)
        {
            if (theLoaiId == null)
            {
                return BadRequest();
            }
            var response = _theLoaiServices.Delete(theLoaiId);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Delete Error");
                return StatusCode(500, ModelState);
            }
            return NoContent();
        }
    }
}
