﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.KhachHangs;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KhachHangController : ControllerBase
    {
        private readonly IKhachHangServices _khachHangServices;

        public KhachHangController(IKhachHangServices khachHangServices)
        {
            _khachHangServices = khachHangServices;
        }

        [HttpGet]
        [Route("ConfirmKhachHang/{khachHangId}")]
        public IActionResult ConfirmKhachHang(int khachHangId)
        {
            var response = _khachHangServices.ConfirmKhachHang(khachHangId);
            if (!response)
            {
                ModelState.AddModelError($"", "Confirm error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpGet]
        [Route("Filter")]
        public IActionResult Filter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email)
        {
            var khachHangs = _khachHangServices.Filter(cumRapId, ten, soDienThoai, diaChi, email);
            return Ok(khachHangs);
        }

        [HttpGet]
        [Route("FilterWithRowPerPage/{index}")]
        public IActionResult FilterWithRowPerPage(int? cumRapId, string ten, string soDienThoai, string diaChi, string email, int index)
        {
            var khachHangs = _khachHangServices.FilterWithRowPerPage(cumRapId, ten, soDienThoai, diaChi, email, index);
            return Ok(khachHangs);
        }

        [HttpGet]
        [Route("CountKhachHangFilter")]
        public IActionResult CountKhachHangFilter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email)
        {
            int count= _khachHangServices.CountKhachHangFilter(cumRapId, ten, soDienThoai, diaChi, email);
            return Ok(count);
        }

        [HttpGet]
        [Route("GetById/{khachHangId}")]
        public IActionResult GetById(int khachHangId)
        {
            var khachHang = _khachHangServices.GetById(khachHangId);
            return Ok(khachHang);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(CreateKhachHangViewModel khachHang)
        {
            if (khachHang == null)
                return BadRequest();
            var response = _khachHangServices.Create(khachHang);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new Error() { Message = response.ErrrorMessages[0] });
            }
            return Ok(response);
        }

        [HttpPatch]
        [Route("Update")]
        public IActionResult Update(KhachHangViewModel khachHang)
        {
            if (khachHang == null)
                return BadRequest();
            var response = _khachHangServices.Update(khachHang);
            if (!response.IsSuccessed)
            {
                ModelState.AddModelError("", $"Update Error !");
                return StatusCode(500, ModelState);
            }
            return Ok();
        }

        [HttpPost]
        [Route("ChangePassword")]
        public  IActionResult ChangePassword(ChangePassword resetPassword)
        {
            if (resetPassword == null)
            {
                return BadRequest(ModelState);
            }
            var response = _khachHangServices.ResetPassword(resetPassword);
            if (!response.IsSuccessed)
            {
                return StatusCode(500, new Error() { Message = response.ErrrorMessages[0] });
            }
            return Ok();
        }

        [HttpPost]
        [Route("Login")]
        public IActionResult Login(KhachHangLoginViewModel khachHang)
        {
            var response = _khachHangServices.Login(khachHang.Email, khachHang.Password);
            return Ok(response);
        }
    }
}
