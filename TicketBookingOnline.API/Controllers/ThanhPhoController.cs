﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Services.IServices;

namespace TicketBookingOnline.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThanhPhoController : ControllerBase
    {
        private readonly IThanhPhoServices _thanhPhoServices;

        public ThanhPhoController(IThanhPhoServices thanhPhoServices)
        {
            _thanhPhoServices = thanhPhoServices;
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var thanhPhos = _thanhPhoServices.GetAll();
            return Ok(thanhPhos);
        }
    }
}
