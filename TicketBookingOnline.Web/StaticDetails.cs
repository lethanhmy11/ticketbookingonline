﻿namespace TicketBookingOnline.Web
{
    public static class StaticDetails
    {
        public static string APIBaseUrl = "https://localhost:44327/";
        public static string PhimAPIPath = APIBaseUrl + "api/Phim/";
        public static string TheLoaiAPIPath = APIBaseUrl + "api/TheLoai/";
        public static string CanhBaoAPIPath = APIBaseUrl + "api/CanhBao/";
        public static string PhimTheLoaiAPIPath = APIBaseUrl + "api/PhimTheLoai/";
        public static string CumRapAPIPath = APIBaseUrl + "api/CumRap/";
        public static string RapAPIPath = APIBaseUrl + "api/Rap/";
        public static string RapGheAPIPath = APIBaseUrl + "api/RapGhe/";
        public static string LichChieuAPIPath = APIBaseUrl + "api/LichChieu/";
        public static string VeAPIPath = APIBaseUrl + "api/Ve/";
        public static string CGVManagerAPIPath = APIBaseUrl + "api/CGVManager/";
        public static string AccountAPIPath = APIBaseUrl + "api/Account/";
        public static string KhachHangAPIPath = APIBaseUrl + "api/KhachHang/";
        public static string CommentAPIPath = APIBaseUrl + "api/Comment/";
        public static string ThongKeAPIPath = APIBaseUrl + "api/ThongKe/";
        public static string ThanhPhoAPIPath = APIBaseUrl + "api/ThanhPho/";
        public static string SliderAPIPath = APIBaseUrl + "api/Slider/";
    }
}
