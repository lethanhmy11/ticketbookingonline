﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Controllers
{
    public class KhuyenMaiController : Controller
    {
        private readonly IPhimServices _phimServices;
        public KhuyenMaiController(IPhimServices phimServices)
        {
            _phimServices = phimServices;
        }

        public async Task<IActionResult> Index(int index = 1)
        {
            ViewBag.Page = index;
            var phimKhuyenMais = await _phimServices.KhuyenMaiPhimAsync(index, StaticDetails.PhimAPIPath + "KhuyenMaiPhim");
            var count = await _phimServices.CountKhuyenMaiPhim(StaticDetails.PhimAPIPath + "CountKhuyenMaiPhim");
            ViewBag.MaxPage = (count /8 -(count % 8 == 0 ? 1 : 0));
            return View(phimKhuyenMais);

        }
    }
}
