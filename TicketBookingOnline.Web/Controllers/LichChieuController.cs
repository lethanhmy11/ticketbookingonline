﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.ViewModels.Phims;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Controllers
{
    public class LichChieuController : Controller
    {
        private readonly ILichChieuServices _lichChieuServices;
        private readonly IPhimServices _phimServices;
        private readonly IRapServices _rapServices;
        private readonly ICumRapServices _cumRapServices;
        private readonly IThanhPhoServices _thanhPhoServices;

        public  LichChieuController(ILichChieuServices lichChieuServices, IPhimServices phimServices, IRapServices rapServices,
            ICumRapServices cumRapServices, IThanhPhoServices thanhPhoServices)
        {
            _lichChieuServices = lichChieuServices;
            _phimServices = phimServices;
            _rapServices = rapServices;
            _cumRapServices = cumRapServices;
            _thanhPhoServices = thanhPhoServices;
        }

        public async Task<IActionResult> LichChieu(int phimId, string ngayChieu, int thanhPhoId = 1, int cumRapId = 1)
        {
            DateTime dateTime = DateTime.Now;
            List<string> dates = new List<string>();
            for(int i =0; i <= 7; i++)
            {
                dates.Add(dateTime.AddDays(i).ToString("dd/MM/yyyy"));
            }            
            TempData["NgayChieu"] = dates;

            var dateDefault = new DateTime(0001, 01, 01);

            DateTime? ngayTimKiem = new DateTime();
            if(ngayChieu == null) {
                ngayTimKiem = null;
            
                TempData["NgayChieuTimKiem"] = "";
            }
            else
            {
                ngayTimKiem = DateTime.ParseExact(ngayChieu, "dd/MM/yyyy", null);
                TempData["NgayChieuTimKiem"] = ngayChieu;
            }
            var lichChieus = await _lichChieuServices.FilterAsync(phimId, cumRapId, null, ngayTimKiem,
                StaticDetails.LichChieuAPIPath + "Filter/");
            var t = lichChieus.AsQueryable().ToList();
            var phimVM = await _phimServices.GetPhimById(phimId, StaticDetails.PhimAPIPath + "GetPhimById");
            ViewBag.Phim = (PhimDetailViewModel)phimVM;
            var raps = await _rapServices.GetRapByCumRapIdAsync(cumRapId, StaticDetails.RapAPIPath + "GetRapByCumRapId");
            ViewData["Raps"] = raps.ToList();


            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            TempData["ThanhPhoId"] = thanhPhoId.ToString();
            TempData["TenThanhPho"] = "";
            if (thanhPhoId != 0)
            {
                TempData["TenThanhPho"] = thanhPhos.Where(x => x.ThanhPhoId == thanhPhoId).First().TenThanhPho;
            }
            var cumRaps = await _cumRapServices.GetAllAsync(null, thanhPhoId, StaticDetails.CumRapAPIPath +
                "GetAll");
            ViewBag.CumRap = new SelectList(cumRaps, "CumRapId", "TenRap");
            TempData["CumRapId"] = cumRapId.ToString();
            TempData["TenCumRap"] = "";
            if (cumRapId != 0)
            {
                TempData["TenCumRap"] = cumRaps.Where(x => x.CumRapId == cumRapId).First().TenRap;
            }
            return View(lichChieus);
        }

        [HttpGet]
        public async Task<JsonResult> FilterCumRap(int thanhPhoId)
        {
            var cumRaps = await _cumRapServices.GetAllAsync(null, thanhPhoId, StaticDetails.CumRapAPIPath +
                "GetAll");
            var listCumRap = new SelectList(cumRaps, "CumRapId", "TenRap");
            return Json(new { list = cumRaps.ToList() });
        }
    }
}
