﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.ViewModels.ChiTietVes;
using TicketBookingOnline.ViewModels.Raps;
using TicketBookingOnline.ViewModels.Ves;
using TicketBookingOnline.Web.IServices;
using TicketBookingOnline.Web.Models;

namespace TicketBookingOnline.Web.Controllers
{
    public class VeController : Controller
    {
        private readonly ILichChieuServices _lichChieuServices;
        private readonly IRapServices _rapServices;
        private readonly IRapGheServices _rapGheServices;
        private readonly IPhimServices _phimServices;
        private readonly IVeServices _veServices;
        private readonly ICumRapServices _cumRapServices;

        public VeController(ILichChieuServices lichChieuServices, IRapServices rapServices, IRapGheServices rapGheServices,
            IPhimServices phimServices, IVeServices veServices, ICumRapServices cumRapServices)
        {
            _lichChieuServices = lichChieuServices;
            _rapServices = rapServices;
            _rapGheServices = rapGheServices;
            _phimServices = phimServices;
            _veServices = veServices;
            _cumRapServices = cumRapServices;
        }
        public async Task<IActionResult> ChonGhe(int lichChieuId)
        {
            var lichChieu = await _lichChieuServices.GetLichChieuById(lichChieuId, StaticDetails.LichChieuAPIPath + "GetById");
            //ViewBag.LichChieuId = lichChieuId;
            TempData["LichChieu"] = lichChieu;
            var phim = await _phimServices.GetPhimById(lichChieu.PhimId, StaticDetails.PhimAPIPath + "GetPhimById");
            TempData["Phim"] = phim;
            var rap = await _rapServices.GetRapByIdAsync(lichChieu.RapId, StaticDetails.RapAPIPath + "GetRapById");
            TempData["Rap"] = (RapViewModel)rap;
            var cumRap = await _cumRapServices.GetByIdAsync(rap.CumRapId, StaticDetails.CumRapAPIPath + "GetById");
            TempData["CumRap"] = cumRap;
            var rapGhes = await _rapGheServices.GetRapGheByRapAsync(lichChieu.RapId, StaticDetails.RapGheAPIPath + "GetRapGheByRap");
            foreach (var item in rapGhes)
            {
                item.RapId = lichChieu.RapId;
            }
            List<string> viTriDaDat = await _rapServices.GetViTriDaDatAsync(rap.RapId, lichChieuId, StaticDetails.RapAPIPath + "GetViTriDaDat");
            if (viTriDaDat.Count() == 0)
            {
                TempData["ViTriDaDat"] = viTriDaDat;
            }
            else
            {
                TempData["ViTriDaDat"] = viTriDaDat;
            }
            
            return View(rapGhes.AsQueryable().ToList());
        }

        [HttpPost]
        public async Task<JsonResult> DatVe(int rapId, int lichChieuId, int phimId, int tongTien, string listGhe)
        {
            //if (listGhe != null)
            //{
                var veVM = new CreateVeViewModel();
                veVM.LichChieuId = lichChieuId;
                veVM.TongGiaVe = tongTien;
                veVM.NgayDatVe = DateTime.Now;
                veVM.TrangThai = "Chưa xác nhận";
                //veVM.UserId = 1;
                veVM.UserId = int.Parse(HttpContext.Session.GetString("KhachHangId"));
                veVM.Note = "";

                List<ChiTietVeViewModel> chiTietVes = new List<ChiTietVeViewModel>();
                List<GheViewModel> jsonListGhe = JsonConvert.DeserializeObject<List<GheViewModel>>(listGhe);
                foreach (var item in jsonListGhe)
                {
                    var chiTietVe = new ChiTietVeViewModel();
                    chiTietVe.RapId = rapId;
                    chiTietVe.LoaiGheId = item.LoaiGheId;
                    chiTietVe.ViTri = item.ViTri;
                    chiTietVes.Add(chiTietVe);
                }
                veVM.ChiTietVes = chiTietVes;
                var response = await _veServices.CreateAsync(veVM, StaticDetails.VeAPIPath + "Create");
            
            return Json(new
            {
                status = response
            });
            
        }

        [HttpGet]
        public async Task<IActionResult> DatVeThanhCong()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> LoiData()
        {
            return View();
        }

    }
}
