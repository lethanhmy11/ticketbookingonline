﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.ViewModels.Comments;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Controllers
{
    public class PhimController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPhimServices _phimServices;

        private readonly ICumRapServices _cumRapServices;
        private readonly ILichChieuServices _lichChieuServices;
        private readonly ICommentServices _commentServices;

        public PhimController(IPhimServices phimServices, ICumRapServices cumRapServices,
            ILogger<HomeController> logger, ILichChieuServices lichChieuServices, ICommentServices commentServices)
        {
            _logger = logger;
            _phimServices = phimServices;
            _cumRapServices = cumRapServices;
            _lichChieuServices = lichChieuServices;
            _commentServices = commentServices;
        }

        public async Task<IActionResult> ChiTietPhim(int phimId)
        {
            
            var phim = await _phimServices.GetPhimById(phimId,StaticDetails.PhimAPIPath + "GetPhimById");
            var phimLienQuan = await _phimServices.GetPhimLienQuanAsync(phimId, StaticDetails.PhimAPIPath + "PhimLienQuan");
            ViewBag.PhimLienQuan = phimLienQuan.ToList();
            return View(phim);
        }

        public async Task<IActionResult> Phim(int phimId)
        {
           
            var phim = await _phimServices.GetPhimById(phimId, StaticDetails.PhimAPIPath + "GetPhimByTrangThaiRowPerPage");

            return View(phim);
        }

        public async Task<JsonResult> AddComment(string CommentText, int PhimId)
        {
            var newComment = new CreateCommentViewModel()
            {
                UserId = int.Parse(HttpContext.Session.GetString("KhachHangId")),
                NoiDung = CommentText,
                PhimId = PhimId
            };
            var comment = await _commentServices.CreateAsync(newComment, StaticDetails.CommentAPIPath + "Create");
            return Json(comment);
        }
    }
}
