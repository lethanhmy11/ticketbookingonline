﻿
function loadData(id) {
    $.ajax({
        type: 'GET',
        data: { "id": id },
        url: '/Admin/LichChieu/Create',
        success: function (response) {
            $('#phimId').val(response.phim.phimId);
            $('#tenPhim').html(response.phim.tenPhim);
            $('#theLoais').html(response.phim.theLoais);
            $('#thoiLuong').html(response.phim.thoiLuong);
            $('#khoiChieu').html(response.khoiChieu);
            $('#daoDien').html(response.phim.daoDien);
            $('#dienVien').html(response.phim.dienVien);
            $('#canhBaoId').html(response.phim.canhBao.canhBaoId);
            $('#canhBaoAnh').attr("src", "/canhbaos/" + response.phim.canhBao.anh);
            $('#canhBaoTen').html(response.phim.canhBao.tenCanhBao);
            $('#totalRate').html(response.totalRate);
            $('#anh').attr("src", "/posters/" + response.phim.anh);
            $('#xuatChieu').attr("min", response.minDate);
            var day = response.khoiChieu.getDate();
            var month = response.khoiChieu.getMonth();
            var year = response.khoiChieu.getYear();
            var khoiChieu = day + "/" + month + "/" + year;
            //$('#theLoais').val(response.theLoais);
            //$('#thoiLuong').val(response.thoiLuong);
            //$('#daoDien').val(response.daoDien);
            //$('#dienVien').val(response.dienVien);
            //$('#canhBaoId').val(response.canhBao.canhBaoId);
            //$('#canhBaoAnh').val(response.canhBao.anh);
            //$('#canhBaoTen').val(response.canhBao.tenCanhBao);
            //$('#totalRate').val(response.totalRate);
        },
        error: function (response) {
            console.log(xhr.responseText);
            alert("Error has occurred..");
        }
    });
}

