﻿function ChooseCity(thanhPhoId) {
    $.ajax({
        type: 'GET',
        data: { "thanhPhoId": thanhPhoId.value },
        url: '/Admin/CGVManager/FilterCumRap',
        success: function (response) {
            $('#cumRapId').empty();

            $.each(response.list, function (key, value) {
                $("#cumRapId").append($('<option>',{value : value.cumRapId, text: value.tenRap}));
            })
        },
        error: function (response) {
            console.log(xhr.responseText);
            alert("Error has occurred..");
        }
    });
}