﻿namespace TicketBookingOnline.Web
{
    public class TestMiddleware
    {
        private readonly RequestDelegate _next;

        public TestMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ILoggerFactory loggerFactory)
        {
            IReadOnlyDictionary<string, object> routeValues = ((dynamic)context.Request).RouteValues as IReadOnlyDictionary<string, object>;
            if(routeValues.Count > 0)
            {
                
                string controllerName = routeValues["controller"].ToString();
                ILogger logger = loggerFactory.CreateLogger<TestMiddleware>();
                logger.LogInformation("LOGGER");
            }
            await _next(context);
        }
    }
}
