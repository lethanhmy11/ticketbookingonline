﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.Comments;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class CommentServices : ICommentServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public CommentServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> ConfirmComment(int commentId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + commentId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteComment(int commentId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + commentId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<CommentViewModel> CreateAsync(CreateCommentViewModel cmtVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cmtVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<CommentViewModel>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<CommentViewModel>> GetBinhLuanByPhimId(int id, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + id);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<CommentViewModel>>(jsonString);
            }
            return null;
        }
    }
}
