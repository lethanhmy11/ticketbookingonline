﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.PhimTheLoais;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class PhimTheLoaiServices : IPhimTheLoaiServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public PhimTheLoaiServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> CreateAsync(List<PhimTheLoaiViewModel> phimTheLoaiVMs, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(phimTheLoaiVMs), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
