﻿using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using TicketBookingOnline.ViewModels.RapGhes;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class RapGheServices : IRapGheServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public RapGheServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }
        public async Task<IEnumerable<RapGheViewModel>> GetRapGheByRapAsync(int RapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + RapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<RapGheViewModel>>(jsonString);
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> UpdateRange(List<RapGheViewModel> rapGhes, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(rapGhes), Encoding.UTF8, "aplication/json");
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}
