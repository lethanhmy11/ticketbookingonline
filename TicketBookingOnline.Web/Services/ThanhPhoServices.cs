﻿using Newtonsoft.Json;
using TicketBookingOnline.ViewModels.ThanhPhos;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class ThanhPhoServices : IThanhPhoServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public ThanhPhoServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<IEnumerable<ThanhPhoViewModel>> GetAllAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<ThanhPhoViewModel>>(jsonString);
            }
            return null;
        }
    }
}
