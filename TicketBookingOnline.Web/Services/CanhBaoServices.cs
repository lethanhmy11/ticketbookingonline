﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels.CanhBaos;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class CanhBaoServices :ICanhBaoServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public CanhBaoServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> CreateAsync(CanhBaoViewModel canhBaoVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(canhBaoVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteAsync(int canhBaoId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url + "/" + canhBaoId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<CanhBaoViewModel>> GetAllAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<CanhBaoViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<CanhBaoViewModel> GetByIdAsync(int canhBaoId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + canhBaoId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<CanhBaoViewModel>(jsonString);
            }
            return null;
        }

        public async Task<bool> UpdateAsync(CanhBaoViewModel canhBaoVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(canhBaoVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}
