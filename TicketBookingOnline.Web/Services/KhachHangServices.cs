﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class KhachHangServices : IKhachHangServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public KhachHangServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<string> ChangePassword(ChangePassword user, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "Success";
            }
            var jsonString = await respone.Content.ReadAsStringAsync();
            var errorMessages = JsonConvert.DeserializeObject<Error>(jsonString);
            return errorMessages.Message;
        }

        public async Task<bool> ConfirmKhachHang(int khachHangId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + khachHangId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<int> CountKhachHangFilterAsync(int? cumRapId, string ten, string soDienThoai, string diaChi, string email, string url)
        {
            url = url + "?";
            if (cumRapId != null)
                url = url + "&cumRapId=" + cumRapId;
            if (ten != null)
                url = url + "&ten=" + ten;
            if (soDienThoai != null)
                url = url + "&soDienThoai=" + soDienThoai;
            if (diaChi != null)
                url = url + "&diaChi=" + diaChi;
            if (email != null)
                url = url + "&email=" + email;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<string> DangKyAsync(CreateKhachHangViewModel khachHangVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(khachHangVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "Success";
            }
            var jsonString = await respone.Content.ReadAsStringAsync();
            var errorMessages = JsonConvert.DeserializeObject<Error>(jsonString);
            return errorMessages.Message;
        }

        public async Task<IEnumerable<KhachHangViewModel>> FilterAsync(int? cumRapId, string ten, string soDienThoai, 
            string diaChi, string email, string url)
        {
            url = url + "?";
            if (cumRapId != null)
                url = url + "&cumRapId=" + cumRapId;
            if (ten != null)
                url = url + "&ten=" + ten;
            if (soDienThoai != null)
                url = url + "&soDienThoai=" + soDienThoai;
            if (diaChi != null)
                url = url + "&diaChi=" + diaChi;
            if (email != null)
                url = url + "&email=" + email;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<KhachHangViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<KhachHangViewModel>> FilterWithRowPerPageAsync(int index, int? cumRapId, string ten, string soDienThoai, string diaChi, string email, string url)
        {
            url = url + "/" + index + "?";
            //url = url + "?";
            if (cumRapId != null)
                url = url + "&cumRapId=" + cumRapId;
            if (ten != null)
                url = url + "&ten=" + ten;
            if (soDienThoai != null)
                url = url + "&soDienThoai=" + soDienThoai;
            if (diaChi != null)
                url = url + "&diaChi=" + diaChi;
            if (email != null)
                url = url + "&email=" + email;
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<KhachHangViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<KhachHangViewModel> GetByIdAsync(int khachHangId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + khachHangId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<KhachHangViewModel>(jsonString);
            }
            return null;
        }

        public async Task<KhachHangViewModel> Login(KhachHangLoginViewModel khachHang, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(khachHang), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<KhachHangViewModel>(jsonString);
            }
            return null;
        }

        public async Task<string> UpdateAsync(KhachHangViewModel khachHangVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(khachHangVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "Success";
            }
            var jsonString = await respone.Content.ReadAsStringAsync();
            var errorMessages = JsonConvert.DeserializeObject<Error>(jsonString);
            return errorMessages.Message;
        }
    }
}
