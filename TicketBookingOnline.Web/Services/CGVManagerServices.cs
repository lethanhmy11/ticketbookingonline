﻿using Newtonsoft.Json;
using System.Text;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.CGVManagers;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Services
{
    public class CGVManagerServices:ICGVManagerServices
    {
        private readonly IHttpClientFactory _clientFactory;

        public CGVManagerServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<int> CountAllAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<int> CountAllNhanVienInCumRapAsync(int cumRapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + cumRapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(jsonString);
            }
            return 0;
        }

        public async Task<string> CreateAsync(CreateCGVManagerViewModel cGVManagerVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cGVManagerVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "Success";
            }
            var jsonString = await respone.Content.ReadAsStringAsync();
            var errorMessages = JsonConvert.DeserializeObject<Error>(jsonString);
            return errorMessages.Message;
        }

        public async Task<bool> DeleteAsync(string id, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url + "/" + id);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                return true;
            }
            return false;
        }

        public async Task<CGVManagerDetailViewModel> FindAsync(string id, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + id);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<CGVManagerDetailViewModel>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<CGVManagerViewModel>> GetAllAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone= await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString= await respone.Content.ReadAsStringAsync(); 
                return JsonConvert.DeserializeObject<IEnumerable<CGVManagerViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<CGVManagerViewModel>> GetNhanVienInCumRapWithRowPerPageAsync(int cumRapId, int index, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + cumRapId + "/" + index);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<CGVManagerViewModel>>(jsonString);
            }
            return null;
        }

        public async Task<IEnumerable<CGVManagerViewModel>> GetWithRowPerPageAsync(int index, int thanhPhoId, int cumRapId, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "/" + index + "/" + thanhPhoId + "/" + cumRapId);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<CGVManagerViewModel>>(jsonString);
            }
            return new List<CGVManagerViewModel>();
        }

        public async Task<bool> UpdateAsync(CGVManagerDetailViewModel cGVManagerVM, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url);
            request.Content = new StringContent(JsonConvert.SerializeObject(cGVManagerVM), Encoding.UTF8, "application/json");
            var client = _clientFactory.CreateClient();
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}
