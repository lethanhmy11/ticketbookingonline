﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.ViewModels.Sliders;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SliderController : Controller
    {
        private readonly ISliderServices _sliderServices;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SliderController(ISliderServices SliderServices, IWebHostEnvironment webHostEnvironment)
        {
            _sliderServices = SliderServices;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            var sliders = await _sliderServices.GetAllAsync(StaticDetails.SliderAPIPath + "GetAll");
            return View(sliders);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SliderViewModel slider, IFormFile anhFile)
        {
            if (anhFile != null)
            {
                string SliderFolder = Path.Combine(_webHostEnvironment.WebRootPath, "sliders");
                string uniqueSlider = Guid.NewGuid().ToString().Substring(0, 8) + "_" + anhFile.FileName;
                string SliderPath = Path.Combine(SliderFolder, uniqueSlider);
                using (var fileStream = new FileStream(SliderPath, FileMode.Create))
                {
                    anhFile.CopyTo(fileStream);
                }
                slider.Anh = uniqueSlider;
            }
            if (ModelState.IsValid)
            {
                var response = await _sliderServices.CreateAsync(slider, StaticDetails.SliderAPIPath + "Create");
                if (response)
                {
                    TempData["Success"] = "Thêm mới thành công !";
                }
                else
                {
                    TempData["Error"] = "Đã xảy ra lỗi ...!";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(slider);
            }

        }
        [HttpGet]
        public async Task<IActionResult> Update(int slideId)
        {
            var slider = await _sliderServices.GetByIdAsync(slideId, StaticDetails.SliderAPIPath + "GetById");
            return View(slider);
        }

        [HttpPost]
        public async Task<IActionResult> Update(SliderViewModel slider, IFormFile anhFile)
        {
            //var sliderUpdate = await _sliderServices.GetByIdAsync(slider.SlideId, StaticDetails.SliderAPIPath + "GetById");
            if (anhFile != null)
            {
                string SliderFolder = Path.Combine(_webHostEnvironment.WebRootPath, "sliders");
                string uniqueSlider = Guid.NewGuid().ToString().Substring(0, 8) + "_" + anhFile.FileName;
                string SliderPath = Path.Combine(SliderFolder, uniqueSlider);
                using (var fileStream = new FileStream(SliderPath, FileMode.Create))
                {
                    anhFile.CopyTo(fileStream);
                }
                slider.Anh = uniqueSlider;
            }
            var response = await _sliderServices.UpdateAsync(slider, StaticDetails.SliderAPIPath + "Update");
            if (response)
            {
                TempData["Success"] = "Cập nhật thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi ...!";
            }
            return RedirectToAction(nameof(Index));

        }

        [HttpPost]
        public async Task<IActionResult> Delete(int slideId)
        {
            var response = await _sliderServices.DeleteAsync(slideId, StaticDetails.SliderAPIPath + "Delete");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi ...!";
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
