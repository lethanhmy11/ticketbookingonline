﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TicketBookingOnline.ViewModels.CumRaps;
using TicketBookingOnline.ViewModels.Raps;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CumRapController : BaseController
    {
        private readonly ICumRapServices _cumRapServices;
        private readonly IRapServices _rapServices;
        private readonly IThanhPhoServices _thanhPhoServices;

        public CumRapController(ICumRapServices cumRapServices, IRapServices rapServices, IThanhPhoServices 
            thanhPhoServices)
        {

            _cumRapServices = cumRapServices;
            _rapServices = rapServices;
            _thanhPhoServices = thanhPhoServices;
        }
        public async Task<IActionResult> Index(string tenCumRap, int thanhPhoId, int index = 1)
        {
            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            var cumRaps = await _cumRapServices.GetWithRowPerPageAsync(index, tenCumRap, thanhPhoId, StaticDetails.CumRapAPIPath + "GetWithRowPerPage");
            int count = 0;
            count = await _cumRapServices.CountAllAsync(tenCumRap, thanhPhoId, StaticDetails.CumRapAPIPath + "CountAll");
            ViewBag.Page = index;
            ViewBag.MaxPage = (count / 20) - (count % 20 == 0 ? 1 : 0);
            ViewBag.TenCumRap = tenCumRap;
            ViewBag.ThanhPhoId = thanhPhoId;
            return View(cumRaps);
        }

        public async Task<IActionResult> Details(int cumRapId)
        {
            var raps = await _rapServices.GetRapByCumRapIdAsync(cumRapId, StaticDetails.RapAPIPath + "GetRapByCumRapId");
            var cumRap = await _cumRapServices.GetByIdAsync(cumRapId, StaticDetails.CumRapAPIPath + "GetById");
            ViewBag.CumRap = cumRap;
            return View(raps);
        }

        public async Task<IActionResult> CreateRap(int createCumRapId, string tenRap, string loaiRap)
        {
            var rapVM = new RapViewModel();
            rapVM.CumRapId = createCumRapId;
            rapVM.TenRap = tenRap;
            rapVM.LoaiRap = loaiRap;
            var response = await _rapServices.CreateAsync(rapVM, StaticDetails.RapAPIPath + "Create");
            if (response)
            {
                TempData["Success"] = "Thêm mới thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction("Details", "CumRap", new { cumRapId = createCumRapId });
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var thanhPhos = await _thanhPhoServices.GetAllAsync(StaticDetails.ThanhPhoAPIPath + "GetAll");
            ViewBag.ThanhPho = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            //ViewData["ThanhPho"] = new SelectList(thanhPhos, "ThanhPhoId", "TenThanhPho");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateCumRapViewModel cumRapVM)
        {
            var response = await _cumRapServices.CreateAsync(cumRapVM, StaticDetails.CumRapAPIPath + "Create");
            if (response)
            {
                TempData["Success"] = "Thêm mới thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCumRap(int cumRapId, string tenRap, string diaChi, string soDienThoai)
        {
            var cumRapVM = new CreateCumRapViewModel();
            cumRapVM.CumRapId = cumRapId;
            cumRapVM.TenRap = tenRap;
            cumRapVM.DiaChi = diaChi;
            cumRapVM.SoDienThoai = soDienThoai;
            var response = await _cumRapServices.UpdateAsync(cumRapVM, StaticDetails.CumRapAPIPath + "Update");
            if (response)
            {
                TempData["Success"] = "Cập nhật thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction("Details", "CumRap", new { cumRapId = cumRapId });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteCumRap(int cumRapId)
        {
            var response = await _cumRapServices.DeleteAsync(cumRapId, StaticDetails.CumRapAPIPath + "Delete");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteRap(int rapId, int cumId)
        {
            var response = await _rapServices.DeleteAsync(rapId, StaticDetails.RapAPIPath + "Delete");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction("Details", "CumRap", new { cumRapId = cumId });
        }
    }
}
