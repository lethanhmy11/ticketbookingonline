﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.ViewModels.RapGhes;
using TicketBookingOnline.ViewModels.Raps;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RapController : BaseController
    {
        private readonly IRapServices _rapServices;
        private readonly IRapGheServices _rapGheServices;

        public RapController(IRapServices rapServices, IRapGheServices rapGheServices)
        {
            _rapServices = rapServices;
            _rapGheServices = rapGheServices;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            int cumRapId = int.Parse(HttpContext.Session.GetString("CumRapId"));
            var raps = await _rapServices.GetRapByCumRapIdAsync(cumRapId, StaticDetails.RapAPIPath + "GetRapByCumRapId");
            return View(raps);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int rapId, int cumRapId)
        {
            ViewBag.CumRapId = cumRapId;
            var rap = await _rapServices.GetRapByIdAsync(rapId, StaticDetails.RapAPIPath + "GetRapById");
            ViewBag.Rap = rap;
            var rapGhes = await _rapGheServices.GetRapGheByRapAsync(rapId, StaticDetails.RapGheAPIPath + "GetRapGheByRap");
            foreach(var item in rapGhes)
            {
                item.RapId = rapId;
            }
            return View(rapGhes.AsQueryable().ToList());
        }

        [HttpPost]
        public async Task<IActionResult> Edit(List<RapGheViewModel> rapGhes, string loaiRap, int rapId)
        {
            var rapUpdate = new RapViewModel();
            rapUpdate.RapId = rapId;
            rapUpdate.LoaiRap = loaiRap;
            var response1 = await _rapServices.UpdateAsync(rapUpdate, StaticDetails.RapAPIPath + "Update");
            var response2 = await _rapGheServices.UpdateRange(rapGhes, StaticDetails.RapGheAPIPath + "UpdateRange");
            if (response1 && response2)
            {
                TempData["Success"] = "Cập nhật thành công !";
                return RedirectToAction(nameof(Edit), new { rapId = rapId});
            }
            var rap = await _rapServices.GetRapByIdAsync(rapId, StaticDetails.RapAPIPath + "GetRapById");
            ViewBag.Rap = rap;
            TempData["Error"] = "Đã xảy ra lỗi!";
            return View(rapGhes);
        }
    }
}
