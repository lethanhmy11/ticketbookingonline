﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.ViewModels.CanhBaos;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CanhBaoController : BaseController
    {
        private readonly ICanhBaoServices _canhBaoServices;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CanhBaoController(ICanhBaoServices canhBaoServices, IWebHostEnvironment webHostEnvironment)
        {
            _canhBaoServices = canhBaoServices;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            var canhBaos = await _canhBaoServices.GetAllAsync(StaticDetails.CanhBaoAPIPath + "GetAll");
            return View(canhBaos);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(string tenCanhBao, IFormFile anhFile)
        {
            var canhBaoVM = new CanhBaoViewModel();
            if (anhFile != null)
            {
                string canhBaoFolder = Path.Combine(_webHostEnvironment.WebRootPath, "canhbaos");
                string uniqueCanhBao = Guid.NewGuid().ToString().Substring(0, 8) + "_" + anhFile.FileName;
                string canhBaoPath = Path.Combine(canhBaoFolder, uniqueCanhBao);
                using (var fileStream = new FileStream(canhBaoPath, FileMode.Create))
                {
                    anhFile.CopyTo(fileStream);
                }
                canhBaoVM.Anh = uniqueCanhBao;
            }
            canhBaoVM.TenCanhBao = tenCanhBao;
            var response = await _canhBaoServices.CreateAsync(canhBaoVM, StaticDetails.CanhBaoAPIPath + "Create");
            if (response)
            {
                TempData["Success"] = "Create successfully !";
            }
            else
            {
                TempData["Error"] = "Some error occured ...!";
            }
            return RedirectToAction(nameof(Index));

        }

        [HttpPost]
        public async Task<IActionResult> Update(int idUpdate, string tenUpdate, IFormFile anhFile)
        {
            var canhBaoVM = await _canhBaoServices.GetByIdAsync(idUpdate, StaticDetails.CanhBaoAPIPath + "GetById");
            if (anhFile != null)
            {
                string canhBaoFolder = Path.Combine(_webHostEnvironment.WebRootPath, "canhbaos");
                string uniqueCanhBao = Guid.NewGuid().ToString().Substring(0, 8) + "_" + anhFile.FileName;
                string canhBaoPath = Path.Combine(canhBaoFolder, uniqueCanhBao);
                using (var fileStream = new FileStream(canhBaoPath, FileMode.Create))
                {
                    anhFile.CopyTo(fileStream);
                }
                canhBaoVM.Anh = uniqueCanhBao;
            }
            canhBaoVM.TenCanhBao = tenUpdate;
            var response = await _canhBaoServices.UpdateAsync(canhBaoVM, StaticDetails.CanhBaoAPIPath + "Update");
            if (response)
            {
                TempData["Success"] = "Update successfully !";
            }
            else
            {
                TempData["Error"] = "Some error occured ...!";
            }
            return RedirectToAction(nameof(Index));

        }

        [HttpPost]
        public async Task<IActionResult> Delete(int canhBaoId)
        {
            var response = await _canhBaoServices.DeleteAsync(canhBaoId, StaticDetails.CanhBaoAPIPath + "Delete");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
            }
            else
            {
                TempData["Error"] = "Đã có lỗi xảy ra ...!";
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
