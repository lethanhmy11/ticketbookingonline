﻿using Microsoft.AspNetCore.Mvc;
using TicketBookingOnline.ViewModels.TheLoais;
using TicketBookingOnline.Web.IServices;

namespace TicketBookingOnline.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TheLoaiController : BaseController
    {
        private readonly ITheLoaiServices _theLoaiServices;

        public TheLoaiController(ITheLoaiServices theLoaiServices)
        {
            _theLoaiServices = theLoaiServices;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var theLoais = await _theLoaiServices.GetAllAsync(StaticDetails.TheLoaiAPIPath + "GetAll");
            return View(theLoais);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        //[HttpPost]
        //public async Task<IActionResult> Create(TheLoaiViewModel theLoai)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var response = await _theLoaiServices.CreateAsync(theLoai, StaticDetails.TheLoaiAPIPath + "Create");
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(theLoai);

        //}

        [HttpPost]
        public async Task<IActionResult> Create(string tenTheLoai)
        {
            var theLoai = new TheLoaiViewModel();
            theLoai.TenTheLoai = tenTheLoai;
            var response = await _theLoaiServices.CreateAsync(theLoai, StaticDetails.TheLoaiAPIPath + "Create");
            if (response)
            {
                TempData["Success"] = "Thêm mới thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Update(int idUpdate, string tenUpdate)
        {
            var theLoai = await _theLoaiServices.GetByIdAsync(idUpdate, StaticDetails.TheLoaiAPIPath + "GetById");
            theLoai.TenTheLoai = tenUpdate;
            var response = await _theLoaiServices.UpdateAsync(theLoai, StaticDetails.TheLoaiAPIPath + "Update");
            if (response)
            {
                TempData["Success"] = "Cập nhật thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int theLoaiId)
        {
            var response = await _theLoaiServices.DeleteAsync(theLoaiId, StaticDetails.TheLoaiAPIPath + "Delete");
            if (response)
            {
                TempData["Success"] = "Xóa thành công !";
            }
            else
            {
                TempData["Error"] = "Đã xảy ra lỗi !";
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
