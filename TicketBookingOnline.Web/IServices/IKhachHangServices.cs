﻿using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.KhachHangs;

namespace TicketBookingOnline.Web.IServices
{
    public interface IKhachHangServices
    {
        Task<int> CountKhachHangFilterAsync(int? cumRapId, string ten, string soDienThoai, string diaChi,
            string email, string url);
        Task<IEnumerable<KhachHangViewModel>> FilterAsync(int? cumRapId, string ten, string soDienThoai, string diaChi,
            string email, string url);
        Task<IEnumerable<KhachHangViewModel>> FilterWithRowPerPageAsync(int index, int? cumRapId, string ten, string soDienThoai,
            string diaChi, string email, string url);
        Task<KhachHangViewModel> GetByIdAsync(int khachHangId, string url);
        Task<bool> ConfirmKhachHang(int khachHangId , string url);
        Task<string> DangKyAsync(CreateKhachHangViewModel khachHangVM, string url);
        Task<string> UpdateAsync(KhachHangViewModel khachHangVM, string url);
        Task<KhachHangViewModel> Login(KhachHangLoginViewModel khachHang, string url);
        Task<string> ChangePassword(ChangePassword user, string url);
    }
}
