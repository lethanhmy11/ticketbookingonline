﻿using TicketBookingOnline.ViewModels.PhimTheLoais;

namespace TicketBookingOnline.Web.IServices
{
    public interface IPhimTheLoaiServices
    {
        Task<bool> CreateAsync(List<PhimTheLoaiViewModel> phimTheLoaiVMs, string url);
    }
}
