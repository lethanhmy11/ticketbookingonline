﻿using TicketBookingOnline.ViewModels.CumRaps;

namespace TicketBookingOnline.Web.IServices
{
    public interface ICumRapServices
    {
        Task<CumRapViewModel> GetByIdAsync(int cumRapId, string url);
        Task<IEnumerable<CumRapViewModel>> GetAllAsync(string tenCumRap, int thanhPhoId, string url);
        Task<int> CountAllAsync(string tenCumRap, int thanhPhoId, string url);
        Task<IEnumerable<CumRapViewModel>> GetWithRowPerPageAsync(int index, string tenCumRap, int thanhPhoId, string url);
        Task<bool> CreateAsync(CreateCumRapViewModel cumRapVM, string url);
        Task<bool> UpdateAsync(CreateCumRapViewModel CumRapVM, string url);
        Task<bool> DeleteAsync(int cumRapId, string url);
    }
}
