﻿using TicketBookingOnline.ViewModels.Comments;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.ThongKes;

namespace TicketBookingOnline.Web.IServices
{
    public interface IThongKeServices
    {
        #region CGV
        Task<DoanhThuTheoNgay> DoanhThuNgayAsync(int ? cumRapId, DateTime ngay, string url);
        Task<DoanhThuTheoTuan> DoanhThuTuanAsync(int ? cumRapId, DateTime ngay, string url);
        Task<DoanhThuTheoThang> DoanhThuThangAsync(int ? cumRapId, int thang, int nam, string url);
        Task<DoanhThuTheoNam> DoanhThuNamAsync(int? cumRapId, int nam, string url);
        Task<IEnumerable<DoanhThuTheoNgay>> ThongKeDoanhThuTuanAsync(int ? cumRapId, DateTime ngay, string url);
        Task<IEnumerable<DoanhThuTheoNgay>> GetAllDoanhThuNgayOfThangInCumRap(int cumRapId, int thang, int nam, string url);
        Task<IEnumerable<DoanhThuTheoTuan>> ThongKeDoanhThuThangAsync(int? cumRapId, DateTime ngay, string url);
        Task<IEnumerable<DoanhThuTheoThang>> ThongKeDoanhThuNamAsync(int? cumRapId, int nam, string url);
        Task<IEnumerable<TrangThaiVe>> ThongKeTrangThaiVeAsync(int? cumRapId, DateTime ngay, string url);
        #endregion

        #region Admin
        Task<IEnumerable<DoanhThuTheoNgay>> GetAllDoanhThuNgayAsync(DateTime ngay, int ? thanhPhoId, string url);
        Task<IEnumerable<DoanhThuTheoThang>> GetAllDoanhThuThangAsync(int thang, int nam, int? thanhPhoId, string url);
        Task<IEnumerable<DoanhThuTheoNam>> GetAllDoanhThuNamAsync(int nam, int? thanhPhoId, string url);
        #endregion

        Task<DashBoard> DashBoardAsync(int ? cumRapId, string url);
        Task<IEnumerable<KhachHangViewModel>> GetKhachHangMoiAsync(int? cumRapId, string url);
        Task<IEnumerable<CommentViewModel>> GetBinhLuanMoiAsync(string url);
    }
}
