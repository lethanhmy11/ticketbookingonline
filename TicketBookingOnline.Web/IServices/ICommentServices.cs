﻿using TicketBookingOnline.ViewModels.Comments;

namespace TicketBookingOnline.Web.IServices
{
    public interface ICommentServices
    {
        Task<bool> ConfirmComment(int commentId, string url);
        Task<bool> DeleteComment(int commentId, string url);
        Task<IEnumerable<CommentViewModel>> GetBinhLuanByPhimId(int id, string url);
        Task<CommentViewModel> CreateAsync(CreateCommentViewModel cmtVM, string url);
    }
}
