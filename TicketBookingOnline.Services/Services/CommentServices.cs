﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Comments;

namespace TicketBookingOnline.Services.Services
{
    public class CommentServices : ICommentServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommentServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool ConfirmComment(int commentId)
        {
            try
            {
                var result = _unitOfWork.CommentRepository.ConfirmComment(commentId);
                _unitOfWork.SaveChanges();
                if (result)
                    return true;
                throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool DeleteComment(int commentId)
        {
            try
            {
                var result = _unitOfWork.CommentRepository.DeleteComment(commentId);
                _unitOfWork.SaveChanges();
                if (result)
                    return true;
                throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<CommentViewModel> Create(CreateCommentViewModel commentVM)
        {
            try
            {
                var comment = _mapper.Map<Comment>(commentVM);
                comment.ThoiGian = DateTime.Now;
                comment.User = _unitOfWork.KhachHangRepository.GetById(commentVM.UserId);
                _unitOfWork.CommentRepository.Create(comment);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CommentViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CommentViewModel>(ex.Message);
            }
        }

        public IEnumerable<CommentViewModel> GetBinhLuanByPhimId(int phimId)
        {
            try
            {
                var binhLuanByPhimIds = _unitOfWork.CommentRepository.GetCommentsByPhimId(phimId);
                var binhLuanByPhimIdVMs = _mapper.Map<IEnumerable<CommentViewModel>>(binhLuanByPhimIds);
                return binhLuanByPhimIdVMs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public CommentViewModel GetCommentLast(int userId)
        {
            var lastCmt = _unitOfWork.CommentRepository.GetLastCmt(userId);
            return _mapper.Map<CommentViewModel>(lastCmt);
        }
    }
}
