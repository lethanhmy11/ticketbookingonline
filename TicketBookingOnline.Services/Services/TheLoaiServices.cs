﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.TheLoais;

namespace TicketBookingOnline.Services.Services
{
    public class TheLoaiServices : ITheLoaiServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TheLoaiServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<TheLoaiViewModel> Create(TheLoaiViewModel theLoaiVM)
        {
            try
            { 
                var theLoai=_mapper.Map<TheLoai>(theLoaiVM);    
                _unitOfWork.TheLoaiRepository.Create(theLoai);
                _unitOfWork.SaveChanges();
                return new ResponseResult<TheLoaiViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<TheLoaiViewModel>(ex.Message);
            }

        }

        public ResponseResult<int> Delete(int theLoaiId)
        {
            try
            {
                _unitOfWork.TheLoaiRepository.Delete(theLoaiId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<TheLoaiViewModel> GetAll()
        {
            try
            {
                var theloais = _unitOfWork.TheLoaiRepository.GetAll().OrderByDescending(x => x.UpdatedDate);
                var theloaiVMs = _mapper.Map<IEnumerable<TheLoaiViewModel>>(theloais);
                return theloaiVMs;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

        }

        public TheLoaiViewModel GetById(int theLoaiId)
        {
            try
            {
                var theloais = _unitOfWork.TheLoaiRepository.GetById(theLoaiId);
                var theloaiVM = _mapper.Map<TheLoaiViewModel>(theloais);
                return theloaiVM;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public ResponseResult<TheLoaiViewModel> Update(TheLoaiViewModel theLoaiVM)
        {
            try
            {
                var theLoai = _mapper.Map<TheLoai>(theLoaiVM);
                _unitOfWork.TheLoaiRepository.Update(theLoai);
                _unitOfWork.SaveChanges();
                return new ResponseResult<TheLoaiViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<TheLoaiViewModel>(ex.Message);
            }
        }
    }
}
