﻿
using AutoMapper;
using System;
using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Helper;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Sliders;

namespace TicketBookingOnline.Services.Services
{
    public class SliderServices : ISliderServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public SliderServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<SliderViewModel> Create(SliderViewModel sliderVM)
        {
            try
            {
                var slider = _mapper.Map<Slider>(sliderVM);
                _unitOfWork.SliderRepository.Create(slider);
                _unitOfWork.SaveChanges();
                return new ResponseResult<SliderViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<SliderViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int sliderId)
        {
            try
            {
                _unitOfWork.SliderRepository.Delete(sliderId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public IEnumerable<SliderViewModel> GetAll()
        {
            try
            {
                var sliders = _unitOfWork.SliderRepository.GetAll();
                return _mapper.Map<IEnumerable<SliderViewModel>>(sliders);
            }
            catch (Exception ex)
            {
               throw new Exception(ex.Message);
            }
        }

        public SliderViewModel GetById(int sliderId)
        {
            try
            {
                var slider = _unitOfWork.SliderRepository.GetById(sliderId);
                return _mapper.Map<SliderViewModel>(slider);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<SliderViewModel> Update(SliderViewModel sliderVM)
        {
            try
            {
                var slider = _unitOfWork.SliderRepository.GetById(sliderVM.SlideId);
                slider.Ten = sliderVM.Ten;
                slider.Anh = sliderVM.Anh;
                _unitOfWork.SliderRepository.Update(slider);
                _unitOfWork.SaveChanges();
                return new ResponseResult<SliderViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<SliderViewModel>(ex.Message);
            }
        }
    }
}
