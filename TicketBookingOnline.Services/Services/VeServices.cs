﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Ves;

namespace TicketBookingOnline.Services.Services
{
    public class VeServices : IVeServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public VeServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ResponseResult<bool> CancelVe(DeleteVeViewModel ve)
        {
            try
            {
                _unitOfWork.VeRepository.CancelVe(ve.VeId, ve.Note);
                _unitOfWork.SaveChanges();
                return new ResponseResult<bool>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<bool>(ex.Message);
            }
        }

        public ResponseResult<bool> ConfirmVe(int veId)
        {
            try
            {
                _unitOfWork.VeRepository.ConfirmVe(veId);
                _unitOfWork.SaveChanges();
                return new ResponseResult<bool>();
            }
            catch(Exception ex)
            {
                return new ResponseResult<bool>(ex.Message);
            }
        }

        public int CountVe(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.Filter(cumRapId, rapId, ngayChieu, tenPhim, userName);
                return ves.Count();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ResponseResult<CreateVeViewModel> Create(CreateVeViewModel veVM)
        {
            try
            {
                var ve = _mapper.Map<Ve>(veVM);
                _unitOfWork.VeRepository.Create(ve);
                var khachHang = _unitOfWork.KhachHangRepository.GetById(veVM.UserId);
                khachHang.DiemThuong = khachHang.DiemThuong + (Convert.ToInt32(veVM.TongGiaVe)/10000);
                _unitOfWork.KhachHangRepository.Update(khachHang);
                _unitOfWork.SaveChanges();
                return new ResponseResult<CreateVeViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateVeViewModel>(ex.Message);
            }
        }

        public IEnumerable<VeViewModel> Filter(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.Filter(cumRapId,rapId, ngayChieu,tenPhim,userName);
                return _mapper.Map<IEnumerable<VeViewModel>>(ves);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<VeViewModel> GetAllIsActive()
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetAllIsActive();
                return _mapper.Map<IEnumerable<VeViewModel>>(ves);

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public VeDetailViewModel GetById(int veId)
        {
            try
            {
                var ve = _unitOfWork.VeRepository.GetDetailById(veId);
                var result = _mapper.Map<VeDetailViewModel>(ve);
                var rap = _unitOfWork.RapRepository.GetById(result.ChiTietVes.FirstOrDefault().RapId);
                result.TenRap = rap.TenRap;
                var cumRap = _unitOfWork.CumRapRepository.GetById(rap.CumRapId);
                result.TenCumRap = cumRap.TenRap;
                result.DiaChi = cumRap.DiaChi;
                return _mapper.Map<VeDetailViewModel>(result);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<VeViewModel> GetVeByKhachHangId(int khachHangId)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.GetVeByKhachHangId(khachHangId);
                return _mapper.Map<IEnumerable<VeViewModel>>(ves);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<VeViewModel> VeWithRowPerPage(int? cumRapId, int? rapId, DateTime? ngayChieu, string tenPhim, string userName, int index)
        {
            try
            {
                var ves = _unitOfWork.VeRepository.VeWithRowPerPage(cumRapId, rapId, ngayChieu, tenPhim, userName, index);
                return _mapper.Map<IEnumerable<VeViewModel>>(ves);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
