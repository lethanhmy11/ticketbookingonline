﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Models;
using TicketBookingOnline.Services.IServices;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Roles;

namespace TicketBookingOnline.Services.Services
{
    public class RoleServices : IRoleServices
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<CGVManager> userManager;

        public RoleServices(RoleManager<IdentityRole> roleManagers, UserManager<CGVManager> userManager)
        {
            this.roleManager = roleManagers;
            this.userManager = userManager;
        }
        public async Task<ResponseResult<string>> CreateAsync(string roleNames)
        {
            try
            {              
                var identityRole = new IdentityRole() { Name = roleNames };
                var result = await roleManager.CreateAsync(identityRole);
                if (!result.Succeeded)
                       return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public async Task<ResponseResult<string>> DeleteAsync(string roleId)
        {
            try
            {
                IdentityRole role = await roleManager.FindByIdAsync(roleId);
                if (role != null)
                {
                    IdentityResult result = await roleManager.DeleteAsync(role);
                    if (!result.Succeeded)
                        return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                }
                else
                    return new ResponseResult<string>("Roles is not exists");
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public async Task<RoleEdit> FindRoleEdit(string id)
        {
            IdentityRole role = await roleManager.FindByIdAsync(id);
            List<CGVManager> members = new List<CGVManager>();
            List<CGVManager> nonMembers = new List<CGVManager>();
            foreach (CGVManager user in userManager.Users)
            {
               
                bool isMember = await userManager.IsInRoleAsync(user, role.Name);
                if(isMember)
                    members.Add(user);
               else
                    nonMembers.Add(user);
            }

            return new RoleEdit
            {
                Role = role,
                Members = members,
                NonMembers = nonMembers
            };

        }

        public IEnumerable<IdentityRole> GetAll()
        {
            return roleManager.Roles;
        }

        public async Task<ResponseResult<string>> UpdateAsync(RoleModification model)
        {
            try
            {
                IdentityResult result;
                foreach (string userId in model.AddIds ?? new string[] { })
                {
                    CGVManager user = await userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        result = await userManager.AddToRoleAsync(user, model.RoleName);
                        if (!result.Succeeded)
                            return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                    }
                }
                foreach (string userId in model.DeleteIds ?? new string[] { })
                {
                    CGVManager user = await userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        result = await userManager.RemoveFromRoleAsync(user, model.RoleName);
                        if (!result.Succeeded)
                            return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                    }

                }
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }
    }
}
