﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Accounts;
using TicketBookingOnline.ViewModels.KhachHangs;

namespace TicketBookingOnline.Services.IServices
{
    public interface IKhachHangServices
    {
        IEnumerable<KhachHangViewModel> Filter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email);
        int CountKhachHangFilter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email);
        IEnumerable<KhachHangViewModel> FilterWithRowPerPage(int? cumRapId, string ten, string soDienThoai, string diaChi, string email, int index);
        KhachHangViewModel GetById(int khachHangId);
        bool ConfirmKhachHang(int userId);
        KhachHangViewModel Login(string email, string password);
        ResponseResult<CreateKhachHangViewModel> Create(CreateKhachHangViewModel khachHangVM);
        ResponseResult<KhachHangViewModel> Update(KhachHangViewModel khachHangVM);
        ResponseResult<ChangePassword> ResetPassword(ChangePassword request);
    }
}
