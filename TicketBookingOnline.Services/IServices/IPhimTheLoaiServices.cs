﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.PhimTheLoais;

namespace TicketBookingOnline.Services.IServices
{
    public interface IPhimTheLoaiServices
    {
        ResponseResult<int> Create(List<PhimTheLoaiViewModel> phimTheLoaiVM);
    }
}
