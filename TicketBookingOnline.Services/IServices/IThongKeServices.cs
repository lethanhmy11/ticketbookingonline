﻿using System;
using System.Collections.Generic;
using TicketBookingOnline.ViewModels.Comments;
using TicketBookingOnline.ViewModels.KhachHangs;
using TicketBookingOnline.ViewModels.ThongKes;

namespace TicketBookingOnline.Services.IServices
{
    public interface IThongKeServices
    {
        DoanhThuTheoNgay DoanhThuNgay(int? cumRapId, DateTime ngay);
        DoanhThuTheoTuan DoanhThuTuan(int? cumRapId, DateTime ngay);
        DoanhThuTheoThang DoanhThuThang(int? cumRapId, int thang, int nam);
        DoanhThuTheoNam DoanhThuNam(int? cumRapId, int nam);
        IEnumerable<DoanhThuTheoNgay> ThongKeDoanhThuTuan(int? cumRapId, DateTime ngay);
        IEnumerable<DoanhThuTheoNgay> GetAllDoanhThuNgayOfThangInCumRap(int cumRapId, int thang, int nam);
        IEnumerable<DoanhThuTheoTuan> ThongKeDoanhThuThang(int? cumRapId, int thang, int nam);
        IEnumerable<DoanhThuTheoThang> ThongKeDoanhThuNam(int? cumRapId, int nam);
        List<TrangThaiVe> ThongKeTrangThaiVe(int? cumRapId, DateTime ngay);
        DashBoard DashBoard(int? cumRapId);
        IEnumerable<KhachHangViewModel> GetKhachHangMoi(int? cumRapId);
        IEnumerable<CommentViewModel> GetBinhLuanMoi();

        #region Admin
        IEnumerable<DoanhThuTheoNgay> GetAllDoanhThuNgay(DateTime ngay);
        #endregion
    }
}
