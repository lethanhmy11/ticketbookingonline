﻿using System.Collections.Generic;
using TicketBookingOnline.ViewModels;
using TicketBookingOnline.ViewModels.Comments;

namespace TicketBookingOnline.Services.IServices
{
    public interface ICommentServices
    {
        bool ConfirmComment(int commentId);
        bool DeleteComment(int commentId);
        ResponseResult<CommentViewModel> Create(CreateCommentViewModel commentVM);
        IEnumerable<CommentViewModel> GetBinhLuanByPhimId(int phimId);
        CommentViewModel GetCommentLast(int userId);
    }
}
