﻿using System;

namespace TicketBookingOnline.Core.BaseEntities
{
    public class BaseEntity:IBaseEntity
    {
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
