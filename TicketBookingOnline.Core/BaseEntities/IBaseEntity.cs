﻿using System;


namespace TicketBookingOnline.Core.BaseEntities
{
    public interface IBaseEntity
    {
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
