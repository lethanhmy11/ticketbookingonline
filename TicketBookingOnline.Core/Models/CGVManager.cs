﻿using Microsoft.AspNetCore.Identity;
using System;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    public class CGVManager: IdentityUser
    {
        public int CumRapId { set; get; }
        public string Image { set; get; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
