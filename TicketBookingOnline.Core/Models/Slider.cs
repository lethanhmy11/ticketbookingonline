﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("Slider")]
    public class Slider:BaseEntity
    {
        [Key]
        public int SlideId { get; set; }
        public string Ten { get; set; }
        public string Anh { get; set; }
    }
}
