﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    public class RapGhe : BaseEntity
    {
        [Key]
        public int RapGheId { get; set; }

        public int RapId { get; set; }
        
        public int LoaiGheId { get; set; }

        public decimal GiaGhe { get; set; }
        public int SoLuongGhe { get; set; }
        
        [ForeignKey("RapId")]
        public Rap Rap { get; set; }

        [ForeignKey("LoaiGheId")]
        public LoaiGhe LoaiGhe { get; set; }

        public ICollection<ChiTietVe> ChiTietVes { get; set; }
    }
}
