﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("LoaiGhe")]
    public class LoaiGhe : BaseEntity
    {
        [Key]
        public int LoaiGheId { get; set; }
        public string TenLoaiGhe { get; set; }

        
        public ICollection<RapGhe> RapGhes { get; set; }
    }
}
