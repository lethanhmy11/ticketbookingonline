﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("Phim")]
    public class Phim : BaseEntity
    {
        [Key]
        public int PhimId { get; set; }
        public string TenPhim { get; set; }
        public int ThoiLuong { get; set; }
        public string Anh { get; set; }
        public string Banner { get; set; }
        public string Trailer { get; set; }
        public string MoTa { get; set; }
        public DateTime KhoiChieu { get; set; }
        public string NgonNgu { get; set; }
        public string DaoDien { get; set; }
        public string DienVien { get; set; }
        public int CanhBaoId { get; set; }
        public string TrangThai { get; set; }
        public int SoLuongLichChieu { get; set; }

        public float TotalRate { get; set; }
        public float KhuyenMai { get; set; }

        public decimal GiaVe { get; set; }

        [ForeignKey("CanhBaoId")]
        public CanhBao CanhBao { get; set; }
        public ICollection<PhimTheLoai> PhimTheLoais { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<LichChieu> LichChieus { get; set; }
        public ICollection<RapPhim> RapPhims { get; set; }
    }
}
