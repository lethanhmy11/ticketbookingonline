﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketBookingOnline.Core.Models
{
    [Table("ChiTietVe")]
    public class ChiTietVe
    {
        [Key]
        public int ChiTietVeId { get; set; }

        public int LoaiGheId { get; set; }

        public string ViTri { get; set; }

        public int VeId { get; set; }

        public int RapId { get; set; }

        [ForeignKey("VeId")]
        public Ve Ve { get; set; }

        [ForeignKey("RapId, LoaiGheId")]
        public RapGhe RapGhe {get ; set;}
    }
}
