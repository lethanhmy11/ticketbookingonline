﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("CumRap")]
    public class CumRap : BaseEntity
    {
        [Key]
        public int CumRapId { get; set; }
        public int ThanhPhoId { get; set; }
        public string TenRap { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }

        [ForeignKey("ThanhPhoId")]
        public ThanhPho ThanhPho { get; set; }

        public ICollection<Rap> Raps { get; set; }
    }
}
