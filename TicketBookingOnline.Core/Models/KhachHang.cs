﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("KhachHang")]
    public class KhachHang : BaseEntity
    {
        [Key]
        public int UserId { get; set; }
        public string HoTen { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
        public string Anh { get; set; }
        public string Role { get; set; }        
        public string Password { get; set; }
        public int DiemThuong { get; set; }
        public bool Confirm { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ICollection<Ve> Ves { get; set; }
    }
}
