﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketBookingOnline.Core.BaseEntities;

namespace TicketBookingOnline.Core.Models
{
    [Table("CanhBao")]
    public class CanhBao:BaseEntity
    {
        [Key]
        public int CanhBaoId { get; set; }
        public string TenCanhBao { get; set; }
        public string Anh { get; set; }
        public ICollection<Phim> Phims { get; set; }
    }
}
