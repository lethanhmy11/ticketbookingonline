﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class RapGheRepository: BaseRepository<RapGhe>, IRapGheRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public RapGheRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<RapGhe> GetRapGheByRap(int ? rapId)
        {
            return _context.RapGhes.Include(x => x.LoaiGhe).Where(x => x.RapId == rapId);
        }

        //public void UpdateRange(List<RapGhe> rapGhes)
        //{
        //    _context.RapGhes.UpdateRange(rapGhes);
        //}
    }
}

