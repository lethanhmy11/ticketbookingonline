﻿using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;


namespace TicketBookingOnline.Core.Repositories
{
    public class RapRepository : BaseRepository<Rap>, IRapRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public RapRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Rap> GetRapByCumRapId(int cumRapId)
        {
            return _context.Raps.Where(x => x.CumRapId == cumRapId);
        }

        public List<string> GetViTriDaDat(int rapId, int lichChieuId)
        {
            var ves = _context.Ves.Where(x => x.LichChieuId == lichChieuId && x.ChiTietVes.First().RapId == rapId);
            var result = new List<string>();
            foreach(var item in ves)
            {
                var listChiTietVe = _context.ChiTietVes.Where(x => x.VeId == item.VeId);
                foreach (var chiTietVe in listChiTietVe)
                    result.Add(chiTietVe.ViTri);
            }
            return result;
        }
    }
}
