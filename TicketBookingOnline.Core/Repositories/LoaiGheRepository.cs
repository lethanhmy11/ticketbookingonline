﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class LoaiGheRepository : BaseRepository<LoaiGhe>, ILoaiGheRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public LoaiGheRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }
    }
}
