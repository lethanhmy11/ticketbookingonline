﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{
    public class CumRapRepository : BaseRepository<CumRap>, ICumRapRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public CumRapRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public bool DeleteCumRap(int cumRapId)
        {
            var cumRap = _context.CumRaps.Where(x => x.CumRapId == cumRapId).FirstOrDefault();
            if (cumRap != null)
            {
                cumRap.IsActive = false;
                _context.CumRaps.Update(cumRap);
                return true;
            }
            return false;
        }

        public IEnumerable<CumRap> GetAllDetails()
        {
            var tttt = _context.CumRaps.Include(x => x.ThanhPho).Where(x => x.IsActive == true).Count();
            return _context.CumRaps.Include(x => x.ThanhPho).Where(x=> x.IsActive == true);
        }

        public IEnumerable<CumRap> GetAllDetailWithRowPerPage(int index, string tenCumRap, int ? thanhPhoId)
        {
            int rowPerPage = 20;
            var cumRap = _context.CumRaps.Include(x => x.ThanhPho).Where(x => x.IsActive == true);
            if( tenCumRap != null)
            {
                cumRap = cumRap.Where(x => x.TenRap.ToLower().Contains(tenCumRap.ToLower()));
            }
            if( thanhPhoId != null && thanhPhoId != 0)
            {
                cumRap = cumRap.Where(x => x.ThanhPhoId == thanhPhoId);
            }
            return cumRap.Skip((index - 1)* rowPerPage).Take(rowPerPage).OrderBy(x => x.ThanhPho.TenThanhPho);
        }

        public CumRap GetDetailById(int cumRapId)
        {
            return _context.CumRaps.Include(x => x.ThanhPho).Where(x=>x.CumRapId == cumRapId).FirstOrDefault();
        }
    }
}
