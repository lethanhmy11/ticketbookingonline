﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.IRepositories;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.Repositories
{

    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        private readonly TicketBookingOnlineDBContext _context;

        public CommentRepository(TicketBookingOnlineDBContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Comment> CommentMoi()
        {
            return _context.Comments.Include(x=>x.User).Include(x=>x.Phim)
                .Where(x => x.Confirm == false && x.IsActive == true);
        }

        public bool ConfirmComment(int commentId)
        {
            var comment = _context.Comments.Where(x => x.CommentId == commentId).FirstOrDefault();
            if (comment != null)
            {
                comment.Confirm = true;
                _context.Update(comment);
                return true;
            }
            return false;
        }

        public bool DeleteComment(int commentId)
        {
            var comment = _context.Comments.Where(x => x.CommentId == commentId).FirstOrDefault();
            if (comment != null)
            {
                comment.Confirm = false;
                _context.Update(comment);
                return true;
            }
            return false;
        }

        public IEnumerable<Comment> GetCommentsByPhimId(int phimId)
        {
            return _context.Comments.Include(x => x.User).Where(c => c.PhimId == phimId && c.IsActive == true).OrderByDescending(c => c.ThoiGian).ToList();
        }

        public Comment GetLastCmt(int userId)
        {
            return _context.Comments.Include(x => x.User).Where(x => x.UserId == userId).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }
    }
}
