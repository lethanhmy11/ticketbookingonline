﻿using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface ICommentRepository: IBaseRepository<Comment>
    {
        IEnumerable<Comment> CommentMoi();
        bool ConfirmComment(int commentId);
        bool DeleteComment(int commentId);
        IEnumerable<Comment> GetCommentsByPhimId(int phimId);
        Comment GetLastCmt(int userId);
    }
}
