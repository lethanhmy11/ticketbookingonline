﻿using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface IKhachHangRepository : IBaseRepository<KhachHang>
    {
        IEnumerable<KhachHang> Filter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email);
        IEnumerable<KhachHang> FilterWithRowperPage(int? cumRapId, string ten, string soDienThoai, string diaChi, string email, int index);
        int CountKhachHangFilter(int? cumRapId, string ten, string soDienThoai, string diaChi, string email);
        IEnumerable<KhachHang> KhachHangMoi(int? cumRapId);
        bool CofirmKhachHang(int userId);
        KhachHang Login(string email, string password);
        bool EmailIsExist(string email);
    }
}
