﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;
namespace TicketBookingOnline.Core.IRepositories
{
    public interface  ILichChieuRepository : IBaseRepository<LichChieu>
    {
        IEnumerable<LichChieu> GetLichChieuByPhimId(int phimId);
        IEnumerable<LichChieu> GetLichChieuOfPhimInCumRap(int phimId, int CumRapId);
        IEnumerable<string> GetLichChieuInCumRap(int CumRapId);
        IEnumerable<LichChieu> Filter(int phimId, int ? cumRapId, int ? rapId, DateTime ? ngayChieu);
        bool IsLichChieuExists(int phimId, DateTime xuatChieu, int rapId);
    }
}
