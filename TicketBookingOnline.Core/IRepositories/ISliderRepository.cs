﻿using TicketBookingOnline.Core.Infrastructures;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface ISliderRepository : IBaseRepository<Slider>
    {
        bool DeleteSlider(int sliderId);
    }
}
