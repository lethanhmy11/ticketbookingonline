﻿using System.Collections.Generic;
using TicketBookingOnline.Core.Models;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface ICGVManagerRepository
    {
        void Delete(CGVManager cGVManager);
        IEnumerable<CGVManager> GetNhanVienInCumRapWithRowPerPage(int cumRapId, int index);
        IEnumerable<CGVManager> GetCGVManagerWithRowPerPage(int index, int thanhPhoId, int cumRapId);
        int CountAllNhanVienInCumRap(int cumRapId);
        string RoleName(string userId);
    }
}
