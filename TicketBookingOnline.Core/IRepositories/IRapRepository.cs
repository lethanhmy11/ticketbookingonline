﻿using System.Collections.Generic;
using TicketBookingOnline.Core.Infrastructures;

namespace TicketBookingOnline.Core.IRepositories
{
    public interface  IRapRepository : IBaseRepository<Rap>
    {
        public IEnumerable<Rap> GetRapByCumRapId(int cumRapId);
        public List<string> GetViTriDaDat(int rapId, int lichChieuId);
    }
}
