﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketBookingOnline.Core.Migrations
{
    public partial class UpdateDataVer2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                column: "ConcurrencyStamp",
                value: "65ac5cd4-5c8c-436e-81e0-9a096d9e9f1c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038a11",
                column: "ConcurrencyStamp",
                value: "f0b401fa-553d-42a3-8a4d-c22bbfcf7cce");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038d8b",
                column: "ConcurrencyStamp",
                value: "aff9bd63-d102-49a4-8b24-19582c9fcf44");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f2049908-8a39-4236-bbcc-109f03ced012",
                column: "ConcurrencyStamp",
                value: "226184d0-19fc-4676-98b7-f1f8280f783b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70dc1302-cf04-4f4e-83d7-da3eadd87111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c4a091af-e183-42d8-a5f3-5c5bba2cd440", "AQAAAAEAACcQAAAAEETaAdFbT9qGXXjDydbuF4lllunVICHglLpmPzWQEL4QzmUcqdd9GT3jNJ0CflyA5Q==", "9407acf5-4249-420d-88ad-3605178a4e17" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a27fa5f7-a648-4570-bb86-0a9acee2b111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9a97ed22-7966-44bf-b0e5-7449a2058e64", "AQAAAAEAACcQAAAAEA74O/YcKMuVPGI1urWuuoeuQ+mUlAS1bFwiJktjQA1N0q2jCnuDSfqmrgic26mCZg==", "067b3fc4-5b42-47f4-8ae0-1749fe6181d5" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "792622bb-38ad-4d5f-9427-81091267a5a3", "AQAAAAEAACcQAAAAELhEURl5cjNscXSLXI+6ndB4Ri4mzhQhzcKJpBmP9CJU4qddZo8Ok1opQfYNiPFmIA==", "fdafbd7b-ce48-4ba4-9ba9-358855afd5a8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083222",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2157408c-0eb3-4eda-b0fc-cc62a93d4293", "AQAAAAEAACcQAAAAEIE/9GeQ1JT8RG3dVfzAPFI7bQUat75ur7vIfAmnkZ6ifkOJg1CIjSxN0n02c4aagg==", "de550873-d43d-4b7d-8d07-8f655efa83ac" });

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 38,
                column: "Anh",
                value: "phi-cong-sieu-dang-maverick.jpg");

            migrationBuilder.InsertData(
                table: "Rap",
                columns: new[] { "RapId", "CreatedDate", "CumRapId", "IsActive", "LoaiRap", "TenRap", "UpdatedDate" },
                values: new object[,]
                {
                    { 145, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 24, true, "Rạp 3D", "Rạp 5", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 146, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 24, true, "Rạp 3D", "Rạp 6", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 124, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, true, "Rạp 3D", "Rạp 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 125, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, true, "Rạp 3D", "Rạp 3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 126, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, true, "Rạp 3D", "Rạp 4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 127, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, true, "Rạp 3D", "Rạp 5", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 128, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, true, "Rạp 3D", "Rạp 6", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 129, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, true, "Rạp 3D", "Rạp 1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 130, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, true, "Rạp 3D", "Rạp 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 131, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, true, "Rạp 3D", "Rạp 3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 132, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, true, "Rạp 3D", "Rạp 4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 133, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, true, "Rạp 3D", "Rạp 5", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 123, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, true, "Rạp 3D", "Rạp 1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 135, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, true, "Rạp 3D", "Rạp 1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 136, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, true, "Rạp 3D", "Rạp 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 137, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, true, "Rạp 3D", "Rạp 3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 138, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, true, "Rạp 3D", "Rạp 4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 139, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, true, "Rạp 3D", "Rạp 5", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 140, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, true, "Rạp 3D", "Rạp 6", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 141, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 23, true, "Rạp 3D", "Rạp 1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 142, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 23, true, "Rạp 3D", "Rạp 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 143, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 23, true, "Rạp 3D", "Rạp 3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 134, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, true, "Rạp 3D", "Rạp 6", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 144, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 24, true, "Rạp 3D", "Rạp 4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "RapGhe",
                columns: new[] { "LoaiGheId", "RapId", "CreatedDate", "GiaGhe", "IsActive", "RapGheId", "SoLuongGhe", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, 29, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 85, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 28, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 84, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 28, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 83, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 28, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 82, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 27, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 81, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 26, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 77, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 27, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 79, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 26, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 78, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 29, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 86, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "RapGhe",
                columns: new[] { "LoaiGheId", "RapId", "CreatedDate", "GiaGhe", "IsActive", "RapGheId", "SoLuongGhe", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, 26, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 76, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 25, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 75, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 25, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 74, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 27, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 80, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 29, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 87, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 31, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 92, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 30, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 89, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 30, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 90, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 31, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 91, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 31, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 93, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 32, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 94, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 32, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 95, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 32, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 96, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 33, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 97, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 33, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 98, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 34, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 100, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 34, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 101, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 34, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 102, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 25, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 73, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 30, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 88, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 33, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 99, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 72, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 70, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 13, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 37, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 36, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 35, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 34, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 33, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 32, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 31, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 30, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 29, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 28, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 27, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 26, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 25, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 24, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 23, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 22, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 21, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 20, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 19, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "RapGhe",
                columns: new[] { "LoaiGheId", "RapId", "CreatedDate", "GiaGhe", "IsActive", "RapGheId", "SoLuongGhe", "UpdatedDate" },
                values: new object[,]
                {
                    { 3, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 18, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 17, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 16, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 15, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 14, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 13, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 12, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 11, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 13, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 38, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 13, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 39, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 40, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 41, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 23, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 69, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 23, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 68, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 23, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 67, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 22, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 66, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 22, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 65, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 22, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 64, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 21, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 63, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 21, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 62, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 21, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 61, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 20, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 60, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 20, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 59, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 20, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 58, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 19, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 57, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 71, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 19, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 56, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 54, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 53, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 52, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 17, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 51, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 17, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 50, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 17, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000m, true, 49, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 20000m, true, 48, 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 47, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 46, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 30000m, true, 45, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 44, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15000m, true, 43, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 25000m, true, 42, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 19, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 55, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, true, 10, 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 123);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 124);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 126);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 127);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 128);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 129);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 130);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 131);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 132);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 133);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 134);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 135);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 136);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 137);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 138);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 139);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 140);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 141);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 142);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 143);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 144);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 145);

            migrationBuilder.DeleteData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 146);

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 5 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 6 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 6 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 6 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 7 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 7 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 7 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 8 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 8 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 8 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 9 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 9 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 9 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 10 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 10 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 10 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 11 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 11 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 11 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 12 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 12 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 12 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 13 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 13 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 13 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 14 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 14 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 14 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 15 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 15 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 15 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 16 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 16 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 16 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 17 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 17 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 17 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 18 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 18 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 18 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 19 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 19 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 19 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 20 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 20 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 20 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 21 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 21 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 21 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 22 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 22 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 22 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 23 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 23 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 23 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 24 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 24 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 24 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 25 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 25 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 25 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 26 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 26 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 26 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 27 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 27 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 27 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 28 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 28 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 28 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 29 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 29 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 29 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 30 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 30 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 30 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 31 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 31 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 31 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 32 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 32 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 32 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 33 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 33 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 33 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 1, 34 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 2, 34 });

            migrationBuilder.DeleteData(
                table: "RapGhe",
                keyColumns: new[] { "LoaiGheId", "RapId" },
                keyValues: new object[] { 3, 34 });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                column: "ConcurrencyStamp",
                value: "b33d69e8-879d-4d97-aa48-84a43c75c24e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038a11",
                column: "ConcurrencyStamp",
                value: "c8aaaa2c-6028-4a5b-ae7a-86ae0d6465de");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038d8b",
                column: "ConcurrencyStamp",
                value: "4d47a522-3292-49d7-a397-ae31084074a8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f2049908-8a39-4236-bbcc-109f03ced012",
                column: "ConcurrencyStamp",
                value: "010aa447-1b94-4967-975d-1af3c06a29c1");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70dc1302-cf04-4f4e-83d7-da3eadd87111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c3853cf1-3887-4662-9493-453fe73cdab0", "AQAAAAEAACcQAAAAECz7u2vi2t8YssPxHPLdlfdKmHNkHayMFQnCZLLJUkczgQnqbuP82KK4yw53SZx/5g==", "f43dfaaf-d85c-4032-9e42-5bfe57a1ccc1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a27fa5f7-a648-4570-bb86-0a9acee2b111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d5289623-cb58-4474-ba94-f7e2cc835ce2", "AQAAAAEAACcQAAAAEIymeN69D6ASrjj/mrjg3mIWJyjedHpJWRaMapgYmQyhYSXHR6UlXoABcrUR1qlRiA==", "2a1a9629-cdff-4591-b40d-2596022b3621" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1f204302-1202-42df-91aa-a5bd7fe82c7d", "AQAAAAEAACcQAAAAENrlDzQ4yNzQ4OPTOaR7KdJ94OPX9qdy5zLbb24nzdbKes/OU/MApIOgeu5CxQDEJw==", "22204cd1-7204-4cfb-b958-5794ef8037da" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083222",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "be6fd91a-a2f6-44af-b690-bd021109e483", "AQAAAAEAACcQAAAAEM12SBGl5fUnzHWwiz0b+IhDg4fVbLmDqj0LUkopMj31QitQRnFjNE2ZV03BMnxCbw==", "e2486f50-16d1-40f0-9097-e5a14d63844f" });

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 38,
                column: "Anh",
                value: "batman.jpg");
        }
    }
}
