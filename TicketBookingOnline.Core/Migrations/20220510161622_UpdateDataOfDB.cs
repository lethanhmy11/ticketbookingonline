﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TicketBookingOnline.Core.Migrations
{
    public partial class UpdateDataOfDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                column: "ConcurrencyStamp",
                value: "b33d69e8-879d-4d97-aa48-84a43c75c24e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038a11",
                column: "ConcurrencyStamp",
                value: "c8aaaa2c-6028-4a5b-ae7a-86ae0d6465de");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038d8b",
                column: "ConcurrencyStamp",
                value: "4d47a522-3292-49d7-a397-ae31084074a8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f2049908-8a39-4236-bbcc-109f03ced012",
                column: "ConcurrencyStamp",
                value: "010aa447-1b94-4967-975d-1af3c06a29c1");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70dc1302-cf04-4f4e-83d7-da3eadd87111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "c3853cf1-3887-4662-9493-453fe73cdab0", "AQAAAAEAACcQAAAAECz7u2vi2t8YssPxHPLdlfdKmHNkHayMFQnCZLLJUkczgQnqbuP82KK4yw53SZx/5g==", "0123456789", "f43dfaaf-d85c-4032-9e42-5bfe57a1ccc1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a27fa5f7-a648-4570-bb86-0a9acee2b111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "d5289623-cb58-4474-ba94-f7e2cc835ce2", "AQAAAAEAACcQAAAAEIymeN69D6ASrjj/mrjg3mIWJyjedHpJWRaMapgYmQyhYSXHR6UlXoABcrUR1qlRiA==", "0123456777", "2a1a9629-cdff-4591-b40d-2596022b3621" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "1f204302-1202-42df-91aa-a5bd7fe82c7d", "AQAAAAEAACcQAAAAENrlDzQ4yNzQ4OPTOaR7KdJ94OPX9qdy5zLbb24nzdbKes/OU/MApIOgeu5CxQDEJw==", "0121652459", "22204cd1-7204-4cfb-b958-5794ef8037da" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083222",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "be6fd91a-a2f6-44af-b690-bd021109e483", "AQAAAAEAACcQAAAAEM12SBGl5fUnzHWwiz0b+IhDg4fVbLmDqj0LUkopMj31QitQRnFjNE2ZV03BMnxCbw==", "01230124521", "e2486f50-16d1-40f0-9097-e5a14d63844f" });

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 1,
                column: "KhuyenMai",
                value: 10.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 3,
                column: "KhuyenMai",
                value: 15.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 4,
                column: "KhuyenMai",
                value: 10.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 14,
                column: "KhuyenMai",
                value: 20.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 22,
                column: "KhuyenMai",
                value: 10.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 29,
                column: "KhuyenMai",
                value: 10.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 32,
                column: "KhuyenMai",
                value: 15.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 35,
                column: "KhuyenMai",
                value: 10.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 36,
                column: "KhuyenMai",
                value: 10.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 37,
                column: "KhuyenMai",
                value: 5.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 39,
                column: "KhuyenMai",
                value: 20.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 48,
                column: "KhuyenMai",
                value: 30.0);

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 4,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 5,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 6,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 7,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 8,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 9,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 10,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 11,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 12,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 13,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 14,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 15,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 16,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 17,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 18,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 19,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 20,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 21,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 22,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 23,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 24,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 25,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 26,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 27,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 28,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 29,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 30,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 31,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 32,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 33,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 34,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 35,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 36,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 37,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 38,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 39,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 40,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 41,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 42,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 43,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 44,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 45,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 46,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 47,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 48,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 49,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 50,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 51,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 52,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 53,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 54,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 55,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 56,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 57,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 58,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 59,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 60,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 61,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 62,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 63,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 64,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 65,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 66,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 67,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 68,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 69,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 70,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 71,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 72,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 73,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 74,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 75,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 76,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 77,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 78,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 79,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 80,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 81,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 82,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 83,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 84,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 85,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 86,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 87,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 88,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 89,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 90,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 91,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 92,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 93,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 94,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 95,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 96,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 97,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 98,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 99,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 100,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 101,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 102,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 103,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 104,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 105,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 106,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 107,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 108,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 109,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 110,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 111,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 112,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 113,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 114,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 115,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 116,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 117,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 118,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 119,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 120,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 121,
                column: "LoaiRap",
                value: "Rạp 3D");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 122,
                column: "LoaiRap",
                value: "Rạp 3D");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                column: "ConcurrencyStamp",
                value: "54946eae-6b33-4a4e-8705-e54cc42cc6d1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038a11",
                column: "ConcurrencyStamp",
                value: "6a8c43bb-6bb9-44fd-b828-7f371a3cccf0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1f45ab0a-c177-42da-b07d-671387038d8b",
                column: "ConcurrencyStamp",
                value: "b6f6ec0a-b879-43e5-9a1c-6bbcfaf738d1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f2049908-8a39-4236-bbcc-109f03ced012",
                column: "ConcurrencyStamp",
                value: "53a8175a-024e-42a5-85d9-4676a9f671a2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "70dc1302-cf04-4f4e-83d7-da3eadd87111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "010d9a6a-90e5-4532-ab04-3ee52f76753b", "AQAAAAEAACcQAAAAEOhSGRE/y1p7tRFfRzqtIq9VMvYVTa0Wto6tUkmXA5zOQovvlsKuIV+ZsyOtysnCzA==", null, "fdcfc052-692a-4086-a14b-66df4e5db551" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a27fa5f7-a648-4570-bb86-0a9acee2b111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "17f9911c-b9fc-4c08-9790-6dd82fa08da5", "AQAAAAEAACcQAAAAEHKS6QpvwmStvQ6iMPPzUbFVkBDJt2/zWWZQv4zmVr+JMaHBKkSpKCwtad61irFNmg==", null, "8630db80-ad2b-48aa-b67e-6a86c77a9bc2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083111",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "329cb262-0a8b-4b76-9507-917ea5e6eb86", "AQAAAAEAACcQAAAAEPlRpRE49dxuCtQ1rcIHzdqIl8dpyJV4AjopFzhPCXfo++cFMy/UJisi4P7VfsldFg==", null, "1195762f-9437-4641-b242-502da727e62c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "c554fd29-f07b-4d28-aea0-815692083222",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "PhoneNumber", "SecurityStamp" },
                values: new object[] { "c22a4621-a465-48ac-92ec-6a0213e95bb2", "AQAAAAEAACcQAAAAEPbJGQZAb+ZUTS5zLgJtsRuKJhd3xp/v9nP8vH7o23L+gqVgWcOtptq2LzX2JPhIYw==", null, "e717f973-e089-44fe-b0db-30ffd94a4255" });

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 1,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 3,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 4,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 14,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 22,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 29,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 32,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 35,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 36,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 37,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 39,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Phim",
                keyColumn: "PhimId",
                keyValue: 48,
                column: "KhuyenMai",
                value: 0.0);

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 4,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 5,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 6,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 7,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 8,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 9,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 10,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 11,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 12,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 13,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 14,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 15,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 16,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 17,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 18,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 19,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 20,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 21,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 22,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 23,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 24,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 25,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 26,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 27,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 28,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 29,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 30,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 31,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 32,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 33,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 34,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 35,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 36,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 37,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 38,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 39,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 40,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 41,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 42,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 43,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 44,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 45,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 46,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 47,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 48,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 49,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 50,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 51,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 52,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 53,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 54,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 55,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 56,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 57,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 58,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 59,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 60,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 61,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 62,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 63,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 64,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 65,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 66,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 67,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 68,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 69,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 70,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 71,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 72,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 73,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 74,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 75,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 76,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 77,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 78,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 79,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 80,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 81,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 82,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 83,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 84,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 85,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 86,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 87,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 88,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 89,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 90,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 91,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 92,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 93,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 94,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 95,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 96,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 97,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 98,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 99,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 100,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 101,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 102,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 103,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 104,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 105,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 106,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 107,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 108,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 109,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 110,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 111,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 112,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 113,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 114,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 115,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 116,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 117,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 118,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 119,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 120,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 121,
                column: "LoaiRap",
                value: "");

            migrationBuilder.UpdateData(
                table: "Rap",
                keyColumn: "RapId",
                keyValue: 122,
                column: "LoaiRap",
                value: "");
        }
    }
}
