﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TicketBookingOnline.Helper
{
    public static class Validation
    {
        public static bool PhoneNumberValidate(this string phoneNumber)
        {
            if (phoneNumber.Length != 10)
            {
                return false;
            }
            if(Int64.TryParse(phoneNumber, out long phone))
            {
                if (phoneNumber.Substring(0, 1).Equals("0"))
                    return true;
                return false;
            }
            return false;
        }

        public static bool EmailValidate(this string email)
        {
            var check = @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$";
            bool isValid = Regex.IsMatch(email, check, RegexOptions.IgnoreCase);
            return isValid;
        }
    }
}
